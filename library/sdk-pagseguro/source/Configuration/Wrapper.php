<?php
/**
 * 2007-2016 [PagSeguro Internet Ltda.]
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author    PagSeguro Internet Ltda.
 * @copyright 2007-2016 PagSeguro Internet Ltda.
 * @license   http://www.apache.org/licenses/LICENSE-2.0
 *
 */

namespace PagSeguro\Configuration;

class Wrapper
{
    const ENV = "sandbox";
    const EMAIL = "matiussifa@gmail.com";
    const TOKEN_PRODUCTION = "your_production_token";
    const TOKEN_SANDBOX = "D9C29B93178E4C028C563C37AAB652BE";
    const APP_ID_PRODUCTION = "your_production_application_id";
    const APP_ID_SANDBOX = "app1596365419";
    const APP_KEY_PRODUCTION = "your_production_application_key";
    const APP_KEY_SANDBOX = "A30A5B59CECEC3E994B96F9C68F90641";
    const CHARSET = "UTF-8";
    const LOG_ACTIVE = false;
    const LOG_LOCATION = "";
}
