<?php

require_once "library/sdk-pagseguro/vendor/autoload.php";

\PagSeguro\Library::initialize();
\PagSeguro\Library::cmsVersion()->setName("Nome")->setRelease("1.0.0");
\PagSeguro\Library::moduleVersion()->setName("Nome")->setRelease("1.0.0");

//Instantiate a new direct payment request, using Credit Card
$creditCard = new \PagSeguro\Domains\Requests\DirectPayment\CreditCard();

/**
 * @todo Change the receiver Email
 */
$creditCard->setReceiverEmail('matiussifa@gmail.com');

// Set a reference code for this payment request. It is useful to identify this payment
// in future notifications.
$creditCard->setReference('REF #A');

// Set the currency
$creditCard->setCurrency("BRL");

// Add an item for this payment request

/*$creditCard->addItems()->withParameters(
    'REF #A',
    'Doação Única (Cartão de Crédito)',
    2,
    50.00

);*/
//Adicionando itens...
ShoppingCart::load();

$cartItems = ShoppingCart::get();
$total=0;
foreach($cartItems as $cartItem){
  $creditCard->addItems()->withParameters(
      'REF #A',
      $cartItem['name'] . ' (Cartão de Crédito)',
      $cartItem['quantity'],
      $cartItem['price']
  );


$total+= $cartItem['quantity'] * $cartItem['price'];
}
//Calculando frete...
AddressActive::load();
$addressActive = AddressActive::get();

$cep = $addressActive->getCep();

$cep = trim($cep);
$cep = str_replace("-", "", $cep);

$_args = array(
       'nCdServico' => '40010',
       'sCepOrigem'=>'79600080', 'sCepDestino' => $cep,
       'nVlPeso' => '1', 'nVlComprimento' => 30,
       'nVlAltura' => 15, 'nVlLargura' => 20  );

$calculafrete = new CalculaFrete( $_args );

$valorFrete = $calculafrete->request();

$valorFrete1 = $valorFrete->Valor;

$valorFreteTratado = str_replace(",", ".", $valorFrete1);

$valorFreteDouble = floatval($valorFreteTratado);


$creditCard->setExtraAmount($valorFreteDouble);

$total+= $valorFreteDouble;

$cpf = str_replace(".", "", $data['creditCardHolderCPF']);
$cpf = str_replace("-", "", $cpf);


// Set your customer information.
// If you using SANDBOX you must use an email @sandbox.pagseguro.com.br
$creditCard->setSender()->setName($data['name']);
$creditCard->setSender()->setEmail($data['email']);

$creditCard->setSender()->setPhone()->withParameters(
    $data['creditCardHolderAreaCode'],
    $data['creditCardHolderPhone']
);

$creditCard->setSender()->setDocument()->withParameters(
    'CPF',
     $cpf
);

$creditCard->setSender()->setHash($data['senderHash']);

$creditCard->setSender()->setIp('172.21.6.16');


// Set shipping information for this payment request
$creditCard->setShipping()->setAddress()->withParameters(
    $data['billingAddressStreet'],
    $data['billingAddressNumber'],
    $data['billingAddressDistrict'],
    $data['billingAddressPostalCode'],
    $data['billingAddressCity'],
    trim(strtoupper($data['billingAddressState'])),
    $data['billingAddressCountry']
);

//Set billing information for credit card
$creditCard->setBilling()->setAddress()->withParameters(
    $data['billingAddressStreet'],
    $data['billingAddressNumber'],
    $data['billingAddressDistrict'],
    $data['billingAddressPostalCode'],
    $data['billingAddressCity'],
    trim(strtoupper($data['billingAddressState'])),
    $data['billingAddressCountry']
);

// Set credit card token
$creditCard->setToken($data['creditCardToken']);


/*
// Set the installment quantity and value (could be obtained using the Installments
// service, that have an example here in \public\getInstallments.php)*/
//Parcelando, deixar em apenas uma parcela
$creditCard->setInstallment()->withParameters(1, $total);


// Set the credit card holder information
$creditCard->setHolder()->setBirthdate($data['creditCardHolderBirthDate']);
$creditCard->setHolder()->setName($data['creditCardHolderName']); // Equals in Credit Card

$creditCard->setHolder()->setPhone()->withParameters(
    $data['creditCardHolderAreaCode'],
    $data['creditCardHolderPhone']
);

$creditCard->setHolder()->setDocument()->withParameters(
   'CPF',
     $cpf
);

// Set the Payment Mode for this payment request
$creditCard->setMode('DEFAULT');

// Set a reference code for this payment request. It is useful to identify this payment
// in future notifications.

try {
    //Get the crendentials and register the boleto payment
    $response = $creditCard->register(
        \PagSeguro\Configuration\Configure::getAccountCredentials()
    );

} catch (Exception $e) {

    die($e->getMessage());
}
