<?php

require_once "sdk-pagseguro/vendor/autoload.php";

\PagSeguro\Library::initialize();
\PagSeguro\Library::cmsVersion()->setName("Nome")->setRelease("1.0.0");
\PagSeguro\Library::moduleVersion()->setName("Nome")->setRelease("1.0.0");

//Instantiate a new Boleto Object
$boleto = new \PagSeguro\Domains\Requests\DirectPayment\Boleto();

// Set the Payment Mode for this payment request
$boleto->setMode('DEFAULT');

/**
 * @todo Change the receiver Email
 */

//$boleto->setReceiverEmail('vendedor@lojamodelo.com.br'); 

// Set the currency
$boleto->setCurrency("BRL");

// Add an item for this payment request
$boleto->addItems()->withParameters(
    'REF #A',
    'Doação via Boleto Bancário',
    1,
    $user->plan_total
);

/*
// Add an item for this payment request
$boleto->addItems()->withParameters(
    '0002',
    'Notebook preto',
    2,
    430.00
);
*/


$cpf = str_replace(".", "", $user->boletoHolderCPF);
$cpf = str_replace("-", "", $cpf);

// Set a reference code for this payment request. It is useful to identify this payment
// in future notifications.
$boleto->setReference("REF".$user->getId());

//set extra amount
#$boleto->setExtraAmount(11.5);

// Set your customer information.
// If you using SANDBOX you must use an email @sandbox.pagseguro.com.br
$boleto->setSender()->setName();
$boleto->setSender()->setEmail();

$boleto->setSender()->setPhone()->withParameters(
    /*boletoHolderAreaCode*/,
    /*boletoHolderPhone*/
);


$boleto->setSender()->setDocument()->withParameters(
    'CPF',
    /*boletoHolderCPF*/
);

error_log($data['senderHash']);

$boleto->setSender()->setHash($data['senderHash']);

$boleto->setSender()->setIp($_SERVER['REMOTE_ADDR']);

/*
// Set shipping information for this payment request
$boleto->setShipping()->setAddress()->withParameters(
    'Av. Brig. Faria Lima',
    '1384',
    'Jardim Paulistano',
    '01452002',
    'São Paulo',
    'SP',
    'BRA',
    'apto. 114'
);
*/
// If your payment request don't need shipping information use:
$boleto->setShipping()->setAddressRequired()->withParameters('FALSE');

try {
    //Get the crendentials and register the boleto payment
    $response = $boleto->register(
        \PagSeguro\Configuration\Configure::getAccountCredentials()
    );

    // You can use methods like getCode() to get the transaction code and getPaymentLink() for the Payment's URL.
    //echo "<pre>";
    //print_r($response);
	
} catch (Exception $e) {
    echo "</br> <strong>";
    die($e->getMessage());
}
