</div>
	<!-- END: PAGE CONTAINER -->

	<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-6 -->
<a name="footer"></a>
<footer class="c-layout-footer c-layout-footer-6 c-bg-grey-1">



	<div class="c-postfooter c-bg-dark-2">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12 c-col">
					<p class="c-copyright c-font-grey">2015 &copy; JANGO
						<span class="c-font-grey-3">All Rights Reserved.</span>
					</p>
				</div>
									<div class="col-md-6 col-sm-12 c-col">
						<ul class="list-unstyled list-inline pull-right">
							<li><img class="img-responsive" src="assets/base/img/content/payments/light/1.png" width="60" /></li>
							<li><img class="img-responsive" src="assets/base/img/content/payments/light/2.png" width="60" /></li>
							<li><img class="img-responsive" src="assets/base/img/content/payments/light/22.png" width="60" /></li>
							<li><img class="img-responsive" src="assets/base/img/content/payments/light/5.png" width="60" /></li>
							<li><img class="img-responsive" src="assets/base/img/content/payments/light/6.png" width="60" /></li>
						</ul>
					</div>
							</div>
		</div>
	</div>

</footer>
<!-- END: LAYOUT/FOOTERS/FOOTER-6 -->

	<!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
<div class="c-layout-go2top">
	<i class="icon-arrow-up"></i>
</div><!-- END: LAYOUT/FOOTERS/GO2TOP -->

	<!-- BEGIN: LAYOUT/BASE/BOTTOM -->
    <!-- BEGIN: CORE PLUGINS -->
	<!--[if lt IE 9]>
	<script src="assets/global/plugins/excanvas.min.js"></script>
	<![endif]-->
	<script src="assets/plugins/jquery.min.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery-migrate.min.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery.easing.min.js" type="text/javascript" ></script>
	<script src="assets/plugins/reveal-animate/wow.js" type="text/javascript" ></script>
	<script src="assets/demos/default/js/scripts/reveal-animate/reveal-animate.js" type="text/javascript" ></script>

	<!-- END: CORE PLUGINS -->

			<!-- BEGIN: LAYOUT PLUGINS -->
			<script src="assets/plugins/revo-slider/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
			<script src="assets/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
			<script src="assets/plugins/revo-slider/js/extensions/revolution.extension.slideanims.min.js" type="text/javascript"></script>
			<script src="assets/plugins/revo-slider/js/extensions/revolution.extension.layeranimation.min.js" type="text/javascript"></script>
			<script src="assets/plugins/revo-slider/js/extensions/revolution.extension.navigation.min.js" type="text/javascript"></script>
			<script src="assets/plugins/revo-slider/js/extensions/revolution.extension.video.min.js" type="text/javascript"></script>
			<script src="assets/plugins/revo-slider/js/extensions/revolution.extension.parallax.min.js" type="text/javascript"></script>
			<script src="assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
			<script src="assets/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
			<script src="assets/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
			<script src="assets/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
			<script src="assets/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
			<script src="assets/plugins/smooth-scroll/jquery.smooth-scroll.js" type="text/javascript"></script>
			<script src="assets/plugins/typed/typed.min.js" type="text/javascript"></script>
			<script src="assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
			<script src="assets/plugins/js-cookie/js.cookie.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery.mask.min.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery.blockUI.js" type="text/javascript"></script>
				<!-- END: LAYOUT PLUGINS -->

	<!-- BEGIN: THEME SCRIPTS -->
	<script src="assets/base/js/components.js" type="text/javascript"></script>
	<script src="assets/base/js/components-shop.js" type="text/javascript"></script>
	<script src="assets/base/js/app.js" type="text/javascript"></script>
	<script>
	$(document).ready(function() {
		App.init(); // init core
	});
	</script>
	<!-- END: THEME SCRIPTS -->

			<!-- BEGIN: PAGE SCRIPTS -->
								<script>
			$(document).ready(function() {
    var slider = $('.c-layout-revo-slider .tp-banner');

    var cont = $('.c-layout-revo-slider .tp-banner-container');

    var api = slider.show().revolution({
        sliderType:"standard",
        sliderLayout:"fullscreen",
        responsiveLevels:[2048,1024,778,320],
        gridwidth: [1240, 1024, 778, 320],
        gridheight: [868, 768, 960, 720],
        delay: 15000,
        startwidth:1170,
        startheight: App.getViewPort().height,

        navigationType: "hide",
        navigationArrows: "solo",

        touchenabled: "on",
        navigation: {
            keyboardNavigation:"off",
            keyboard_direction: "horizontal",
            mouseScrollNavigation:"off",
            onHoverStop:"on",
            arrows: {
                style:"circle",
                enable:true,
                hide_onmobile:false,
                hide_onleave:false,
                tmp:'',
                left: {
                    h_align:"left",
                    v_align:"center",
                    h_offset:30,
                    v_offset:0
                },
                right: {
                    h_align:"right",
                    v_align:"center",
                    h_offset:30,
                    v_offset:0
                }
            }
        },

        spinner: "spinner2",

        fullScreenOffsetContainer: '.c-layout-header',

        shadow: 0,

        disableProgressBar:"on",

        hideThumbsOnMobile: "on",
        hideNavDelayOnMobile: 1500,
        hideBulletsOnMobile: "on",
        hideArrowsOnMobile: "on",
        hideThumbsUnderResolution: 0
    });
}); //ready
			</script>
							<!-- END: PAGE SCRIPTS -->
		<!-- END: LAYOUT/BASE/BOTTOM -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-64667612-1', 'themehats.com');
        ga('send', 'pageview');
    </script>
    </body>
</html>
