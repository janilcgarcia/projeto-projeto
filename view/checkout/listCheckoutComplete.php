<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<!-- BEGIN: PAGE CONTENT -->
<div class="c-content-box c-size-lg c-overflow-hide c-bg-white">
	<div class="container">
		<div class="c-shop-order-complete-1 c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
			<div class="c-content-title-1">
				<h3 class="c-center c-font-uppercase c-font-bold">Checkout Completo</h3>
				<div class="c-line-center c-theme-bg"></div>
			</div>
			<div class="c-theme-bg">
				<p class="c-message c-center c-font-white c-font-20 c-font-sbold">
					<i class="fa fa-check"></i> Obrigado. Sua compra foi confirmada.
				</p>
			</div>
			<!-- BEGIN: ORDER SUMMARY -->
			<?
			$checkout = new Checkout();


			?>
			<div class="row c-order-summary c-center">
				<ul class="c-list-inline list-inline">
					<li> <!-- Precisa inserir dados no dado se buscar aqui-->
						<h3>Número da Compra</h3>
						<p>#00000001</p>
					</li>
					<li>
						<h3>Data da Compra</h3>
						<p><?echo date('d-m-y');?></p>
					</li>
					<li>

					</li>
					<li>
						<h3>Método de pagamento</h3>
						<p>Cartão de Crédito</p>
					</li>
				</ul>
			</div>
			<!-- END: ORDER SUMMARY -->

			<!-- BEGIN: ORDER DETAILS -->
			<div class="c-order-details">
				<div class="c-border-bottom hidden-sm hidden-xs">
					<div class="row">
						<div class="col-md-3">
							<h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Produto</h3>
						</div>
						<div class="col-md-5">
							<h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Descrição</h3>
						</div>
						<div class="col-md-2">
							<h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Preço Unitário</h3>
						</div>
						<div class="col-md-2">
							<h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Total</h3>
						</div>
					</div>
				</div>
				<!-- BEGIN: PRODUCT ITEM ROW -->
				<?
					ShoppingCart::load();
					ShoppingCart::get();

					$cartItems = ShoppingCart::get();
					//Variavel para exibir o sub total de produtos
					$subTotal = 0;

					/*Caso nao haja itens no carrinho sera exbibida uma mensagem
					informado que o carrinho esta vazio e exibido um botao para o usuario ir
					a tela Inicial.
					Caso contrario os itens contidos no carrinho serao exibidos.*/
					$show='';
					$showEmpty='';
					if(empty($cartItems)){
						$show = 'hidden';
						$showEmpty='';
					}else{
						$show='';
						$showEmpty='hidden';
					}


					$taxaEntrega='0';
					if(isset($_SESSION['frete'])){
							
							$taxaEntrega = $_SESSION['frete'];
					}

				?>

				<div class="c-border-bottom c-row-item">
					<?php foreach($cartItems as $cartItem) :?>
					<div class="row">
						<div class="col-md-3 col-sm-12 c-image">
							<div class="c-content-overlay">
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="#" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Detalhes</a>
									</div>
								</div>
								<div class="c-bg-img-top-center c-overlay-object" data-height="height">
									<img width="100%" class="img-responsive" src="<?= $cartItem['picture'] ?>">
								</div>
							</div>
						</div>
						<div class="col-md-5 col-sm-8">
							<ul class="c-list list-unstyled">
								<li class="c-margin-b-25"><a href="shop-product-details-2.html" class="c-font-bold c-font-22 c-theme-link"><?= $cartItem['name'] ?></a></li>
								<li class="c-margin-b-10"><?= $cartItem['price']?></li>
								<li>Quantidade X<?= $cartItem['quantity']?></li>
							</ul>
						</div>
						<div class="col-md-2 col-sm-2">
							<p class="visible-xs-block c-theme-font c-font-uppercase c-font-bold">Unit Price</p>
							<p class="c-font-sbold c-font-uppercase c-font-18">R$<?= $cartItem['price']?></p>
						</div>
						<? $totalProduct =  $cartItem['price'] * $cartItem['quantity'];
							$subTotal += $totalProduct;
						?>
						<div class="col-md-2 col-sm-2">
							<p class="visible-xs-block c-theme-font c-font-uppercase c-font-bold">Total</p>
							<p class="c-font-sbold c-font-18">R$<?= $subTotal ?></p>
						</div>
					</div>
					<? endforeach?>
				</div>
				<!-- END: PRODUCT ITEM ROW -->
				<div class="c-row-item c-row-total c-right">
					<ul class="c-list list-unstyled">
						<li>
							<h3 class="c-font-regular c-font-22">Subtotal : &nbsp;
								<span class="c-font-dark c-font-bold c-font-22">R$<?= $subTotal ?></span>
							</h3>
						</li>
						<li>
							<h3 class="c-font-regular c-font-22">Frete : &nbsp;
								<span class="c-font-dark c-font-bold c-font-22">R$<?= $taxaEntrega?></span>
							</h3>
						</li>
						<li>
							<h3 class="c-font-regular c-font-22">Total : &nbsp;
								<span class="c-font-dark c-font-bold c-font-22">R$<?= $subTotal + $taxaEntrega?></span>
							</h3>
						</li>
					</ul>
				</div>
			</div>
			<!-- END: ORDER DETAILS -->
			<!-- BEGIN: CUSTOMER DETAILS -->
			<div class="c-customer-details row" data-auto-height="true">
				<div class="col-md-6 col-sm-6 c-margin-t-20">
					<div data-height="height">
						<h3 class=" c-margin-b-20 c-font-uppercase c-font-22 c-font-bold">Detalhes do cliente</h3>
						<ul class="list-unstyled">
							<li>Nome: Universidade Federal do Mato Grosso do Sul - UFMS</li>
							<li>Telefone: (67) 3509-3400</li>
							<li>Email: <a href="mailto:gab.cptl@ufms.br" class="c-theme-color">gab.cptl@ufms.br</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 c-margin-t-20">
					<div data-height="height">
						<h3 class=" c-margin-b-20 c-font-uppercase c-font-22 c-font-bold">Endereço</h3>
						<ul class="list-unstyled">
							<li>UFMS - CPTL</li>
							<li>
								Av. Cap. Olinto Mancini, 1662 - Jardim Primaveril, Três Lagoas - MS, 79600-080 <br />
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- END: CUSTOMER DETAILS -->
		</div>
	</div>
</div>
