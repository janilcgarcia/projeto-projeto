<?
require('checkout/createSession.php');

setlocale(LC_MONETARY, 'pt_BR');


$loginManager = UserLoginManager::getInstance();
if($loginManager->isLoggedIn()){
?>

<div>
		<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
		<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
			<div class="container">
				<div class="c-page-title c-pull-left">
					<h3 class="c-font-uppercase c-font-sbold">Checkout</h3>
					<h4 class="">Selecione o endereço para entrega</h4>
				</div>
			</div>
		</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->

    <!-- BEGIN: PAGE CONTENT -->
		<div class="c-content-box c-size-lg">
	<div class="container">
      <form action="index.php?controller=Checkout&action=payment" id="accession" method="post" class="c-shop-form-1" role="form">
			<div class="row">
				<!-- BEGIN: ADDRESS FORM -->
				<div class="col-md-7 c-padding-20">
					<!-- BEGIN: BILLING ADDRESS -->

					<h3 class="c-font-bold c-font-uppercase c-font-24">Endereço</h3>
					<? 	AddressActive::load();
							$addressActive = AddressActive::get();
					?>
					<?php
					if($addresses):
						foreach($addresses as $address):
							if($address->getUserId() == $loginManager->getUser()){
					?>

						<div class="form-group col-md-6">
							<div class="c-content-product-2 c-bg-white c-border">
								<div class="c-info" >
									<p class="c-title c-font-16 c-font-slim">Endereço: <?= $address->getStreet()?></p>
									<p class="c-price c-font-14 c-font-slim">Número: <?= $address->getNumber()?></p>
									<p class="c-price c-font-14 c-font-slim">Bairro: <?= $address->getDistrict()?></p>
									<p class="c-price c-font-14 c-font-slim">Cidade: <?= $address->getCity()?></p>
									<p class="c-price c-font-14 c-font-slim">Estado: <?= $address->getState()?></p>
									<p class="c-price c-font-14 c-font-slim">CEP: <?= $address->getCep()?></p>
								</div>
								<div role="group">
									<div class="btn-group btn-group-justified" role="group">
										<?
										$btnColor='';
										$btnText='';
										if(isset($addressActive)){
											if($addressActive->getId() == $address->getId()){
												$btnColor = 'btn-success c-font-white c-bg-green-2-hover c-font-white-hover ';
												$btnText = 'Selecionado';
											}else{
												$btnColor = 'btn-white c-font-grey-3 c-bg-blue-3-hover c-font-white-hover';
											 	$btnText = 'Selecionar';
											}
										}else{
											$btnColor = 'btn-white c-font-grey-3 c-bg-blue-3-hover c-font-white-hover';
											$btnText = 'Selecionar';
										}
										?>
										<a href="index.php?controller=Address&action=select&addressid=<?= $address->getId()?>&id=<?= $loginManager->getUser()?>"  class="btn c-btn-uppercase c-btn-square btn-sm c-btn-product <?=$btnColor?>"><?=$btnText?></a>
										<a href="index.php?controller=Address&action=edit&addressid=<?= $address->getId()?>&id=<?= $loginManager->getUser()?>" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-blue-2-hover c-btn-product">Editar</a>
										<a href="index.php?controller=Address&action=delete&addressid=<?= $address->getId()?>&id=<?= $loginManager->getUser()?>" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Remover</a>
									</div>
								</div>
							</div>
						</div>

						<?php
									}
							endforeach;
					endif;
						?>
						<div class="row">
							<div class="form-group col-md-12" role="group">
								<a href="index.php?controller=Address&action=add&id=<?= $loginManager->getUser()?>" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Novo Endereço</a>
							</div>
						</div>
					<div id="sectiontohide">
          <form action="index.php?controller=Checkout&action=payment" id="accession" method="post" class="c-shop-form-1" role="form">
					<h3 class="c-font-bold c-font-uppercase c-font-24">Dados do Cartão</h3>
					<div class="row">
						<div class="form-group col-md-9">
							<label class="control-label">Número</label>
								 <input type="text"  class="form-control  c-square c-theme creditCardNumber" name="data[cardNumber]" value="" id="cardNumber" onblur="brandCard();" />
						</div>
						<div class="col-md-3"  >
						<label class="control-label">Bandeira</label>
							<span class="form-control c-square">
								<img class="bandeiraCartao" id="bandeiraCartao" height="20" />
							</span>

						</div>
					</div>

					<div class="row">

						<div class="form-group col-md-8">
							<div class="row" id="expiraCartao">
								<div class="form-group col-md-3">
									<label class="control-label">Mês de Vencimento </label>
									<input type="text"  class="form-control c-square c-theme day" name="data[cardExpirationMonth]" value="" id="cardExpirationMonth" maxlength="2" placeholder="<?= date('m')?>" />
								</div>
								<div class="form-group col-md-4">
									<label class="control-label">Ano de Vencimento </label>
									<input type="text"  class="form-control c-square c-theme year" name="data[cardExpirationYear]"  value="" id="cardExpirationYear"  maxlength="4" placeholder="<?= date('Y')?>"/>
								</div>
								<div class="col-md-4" id="cvvCartao">
									<label class="control-label">Código de Segurança</label>
									<input type="text"  class="form-control c-square c-theme creditCardCode" name="data[cardCvv]"  value="" id="cardCvv" maxlength="5" />
								</div>
							</div>
						</div>
					</div>
					<h3 class="c-font-bold c-font-uppercase c-font-24">Dados do Titular do Cartão </h3>

					<div class="row">
						<div class="form-group col-md-12">
							<label class="control-label">Nome (Como está impresso no cartão) </label>

							<input type="text"  class="form-control c-square c-theme" name="data[creditCardHolderName]"  value=""  id="creditCardHolderName" />
						</div>
					</div>

					<div class="row">

						<div class="form-group col-md-12">
							<div class="row" >
								<div class="form-group col-md-4">
									<label class="control-label">CPF (somente n&uacute;meros)</label>
									<input type="text" name="data[creditCardHolderCPF]"  value="" class="form-control c-square c-theme cpf"  id="creditCardHolderCPF" maxlength="14" />
								</div>
								<div class="col-md-8" >
									<label class="control-label">Data de Nascimento do Titular do Cartão </label>

									<input type="text"  class="form-control c-square c-theme date" name="data[creditCardHolderBirthDate]"  value=""  id="creditCardHolderBirthDate" maxlength="10" />
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-12">
							<div class="row" >
								<div class="form-group col-md-4">
									<label class="control-label">DDD</label>
                    <input type="text" name="data[creditCardHolderAreaCode]" value="" id="creditCardHolderAreaCode" class="form-control c-square c-theme  phone_cod" maxlength="2" />
            		</div>
								<div class="col-md-8" >
									<label class="control-label">Número Telefone </label>
                    <input type="text" name="data[creditCardHolderPhone]" value="" id="creditCardHolderPhone" class="form-control c-square c-theme  phone_number" maxlength="10" />
          			</div>
							</div>
						</div>
					</div>
        </form>
				</div>
				</div>
				<!-- END: ADDRESS FORM -->
				<!-- BEGIN: ORDER FORM -->
				<?
					ShoppingCart::load();
					ShoppingCart::get();

					$cartItems = ShoppingCart::get();
					//Variavel para exibir o sub total de produtos
					$subTotal = 0;

					/*Caso nao haja itens no carrinho sera exbibida uma mensagem
					informado que o carrinho esta vazio e exibido um botao para o usuario ir
					a tela Inicial.
					Caso contrario os itens contidos no carrinho serao exibidos.*/
					$show='';
					$showEmpty='';
					if(empty($cartItems)){
						$show = 'hidden';
						$showEmpty='';
					}else{
						$show='';
						$showEmpty='hidden';
					}


					$taxaEntrega='0';
					if(isset($valorFrete)){
							//Obtendo o valor do frete atraves do xml
							$taxaEntrega = $valorFrete->Valor;
					}

				?>
				<div class="col-md-5">
					<div class="c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
					<h1 class="c-font-bold c-font-uppercase c-font-24">Sua Compra</h1>
					<ul class="c-order list-unstyled">
						<li class="row c-margin-b-15">
							<div class="col-md-6 c-font-20"><h2>Produtos</h2></div>
							<div class="col-md-6 c-font-20"><h2>SubTotal</h2></div>
						</li>
						<li class="row c-border-bottom"></li>
						<?php foreach($cartItems as $cartItem) :?>
						<li class="row c-margin-b-15 c-margin-t-15">
							<div class="col-md-6 c-font-20"><a href="shop-product-details.html" class="c-theme-link"><?= $cartItem['name'] ?> X<?= $cartItem['quantity']?></a></div>
							<div class="col-md-6 c-font-20">
								<p class="">R$<?= $cartItem['price']?></p>
							</div>
						</li>
						<? $totalProduct =  $cartItem['price'] * $cartItem['quantity'];
							$subTotal += $totalProduct;
						?>
					<?php endforeach;?>
						<li class="row c-margin-b-15 c-margin-t-15">
							<div class="col-md-6 c-font-20">Subtotal</div>
							<div class="col-md-6 c-font-20">

								<p class=""><span class="c-subtotal">R$<?= $subTotal ?></span></p>
							</div>
						</li>
						<li class="row c-border-top c-margin-b-15"></li>
						<li class="row">
							<div class="col-md-6 c-font-20">Frete</div>
							<div class="col-md-6">

								<div class="row" <?=$show?>>
										<div class="col-md-6 ">
											<label class="control-label c-font-20 c-font-bold">R$<?= $taxaEntrega ?></label>
										</div>
								</div>
							</div>
						</li>
						<li class="row">
							<div class="col-md-6 c-font-20">Pagamento</div>
							<div class="col-md-6">
								<div class="c-radio-list c-shipping-calculator">
									<div class="c-radio">
										<input type="radio" value="20" id="radio11" class="c-radio" name="shipping_price" onclick="show1();">
										<label for="radio11">
											<span class="inc"></span>
											<span class="check"></span>
											<span class="box"></span>
											Boleto
										</label>
									</div>
									<div class="c-radio">
										<input type="radio" value="10" id="radio12" class="c-radio" name="shipping_price" checked="" onclick="show2();">
										<label for="radio12">
											<span class="inc"></span>
											<span class="check"></span>
											<span class="box"></span>
											Cartão de Crédito
										</label>
									</div>
								</div>
							</div>
						</li>
						<li class="row c-margin-b-15 c-margin-t-15">
							<div class="col-md-6 c-font-20">
								<p class="c-font-30">Total</p>
							</div>
							<div class="col-md-6 c-font-20">
								<p class="c-font-bold c-font-30"><span class="c-shipping-total" name="dados_total">R$<?= $subTotal + $taxaEntrega?></span></p>
								<input type="text" class="form-control c-square c-theme hide" id="dados_total" name="dados_total"  />
							</div>
						</li>

						<li class="row c-margin-b-15 c-margin-t-15">
							<div class="form-group col-md-12">
								<div class="c-checkbox">
									<input type="checkbox" id="checkbox1-11" class="c-check">
									<label for="checkbox1-11">
										<span class="inc"></span>
										<span class="check"></span>
										<span class="box"></span>
										Eu li e aceito os Termos e Condições
									</label>
								</div>
							</div>
						</li>

            <li class="row">
              <!--Mudar aqui responsavel pelo usuario -->
              <input type="hidden"  class="form-control c-square c-theme hide" name="data[name]" value="Guilherme Matiussi Souza" />
              <input type="hidden"  class="form-control c-square c-theme hide" name="data[email]" value="c69933031758525047087@sandbox.pagseguro.com.br" />
              <input type="hidden"  class="form-control c-square c-theme hide"name="data[billingAddressStreet]"  value="<?= $addressActive->getStreet()?>" id="billingAddressStreet" />
              <input type="hidden"  class="form-control c-square c-theme cep hide"name="data[billingAddressPostalCode]"  value="<?= $addressActive->getCep()?>" id="billingAddressPostalCode" maxlength="9" />
              <input type="hidden"  class="form-control c-square c-theme num hide" name="data[billingAddressNumber]"  value="<?= $addressActive->getNumber()?>" id="billingAddressNumber" maxlength="7" />
              <input type="hidden"  class="form-control c-square c-theme hide"name="data[billingAddressDistrict]"  value="<?= $addressActive->getDistrict()?>" id="billingAddressDistrict" />
              <input type="hidden"  class="form-control c-square c-theme hide"name="data[billingAddressComplement]"  value="" id="billingAddressComplement" />
              <input type="hidden"  class="form-control c-square c-theme hide"name="data[billingAddressCity]"  value="<?= $addressActive->getCity()?>" id="billingAddressCity" />
              <input type="hidden"  class="form-control c-square c-theme uf hide" name="data[billingAddressState]"  value="<?= $addressActive->getState()?>" id="billingAddressState"  maxlength="2" style="text-transform: uppercase;" placeholder="UF" />
              <input type="hidden"  class="form-control c-square c-theme hide"name="data[billingAddressCountry]" id="billingAddressCountry" value="BRA" readonly />
              <input type="hidden" name="data[creditCardToken]" id="creditCardToken"  />
					    <input type="hidden" name="data[creditCardBrand]" id="creditCardBrand"  />
            </li>

						<li class="row">
							<div class="form-group col-md-12" role="group">
                <input type="hidden" name="data[senderHash]" id="senderHash"  />
								<button type="submit" id="pagseguro" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold" onclick="payment(); ">Confirmar </button>
							</div>
						</li>
					</ul>
				</div>
				</div>
				<!-- END: ORDER FORM -->
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
		function show1(){
		document.getElementById('sectiontohide').style.display ='none';
		}
		function show2(){
		document.getElementById('sectiontohide').style.display = 'block';
		}

</script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>

<script type="text/javascript">

 	PagSeguroDirectPayment.setSessionId('<?= $sessionCode->getResult() ?>');

</script>
<script>
	$(document).ready(function(){
	  $('.date').mask('00/00/0000');
	  $('.cep').mask('00000000');
	  $('.phone_number').mask('0000000000');
		$('.phone_cod').mask('00');
	  $('.cep').mask('99999-999');
		$('.num').mask('0000000');
		$('.creditCardNumber').mask('0000000000000000');
		$('.creditCardCode').mask('00000');
		$('.day').mask('00');
		$('.year').mask('0000');
		$('.uf').mask('AA');
	  $('.cpf').mask('000.000.000-00');
		$('.money').mask('000.000.000.000.000,00', {reverse: true});
	});
</script>

<script type="text/javascript">
function payment()
	{
		$.blockUI({ message: '<h3><img src="assets/base/img/wait.gif"/> &nbsp; Processando...</h3>' });

		PagSeguroDirectPayment.createCardToken({
			cardNumber: $("#cardNumber").val(),
			brand: $("#creditCardBrand").val(),
			cvv: $("#cardCvv").val(),
			expirationMonth: $("#cardExpirationMonth").val(),
			expirationYear: $("#cardExpirationYear").val(),
			success: function (response) {
			  	$("#creditCardToken").val(response.card.token);
			  	$("#senderHash").val(PagSeguroDirectPayment.getSenderHash());
				$("#accession").submit();
				alert('Sucesso: ' + response.card.token);
				$.unblockUI();
        },
        error: function (response) {
          if (response.error) {

			  alert("Não foi possível validar o cartão. Preencha corretamente os dados e tente novamente.");

			  $.unblockUI();

			}else{
				alert('Erro: ' + response.card.token);
			}
        },
        complete: function (response) {
        }
      });
	}

	function brandCard() {

		if($("#cardNumber").val().length == 16)
		{

			$.blockUI({ message: '<h3><img src="assets/base/img/wait.gif" /> &nbsp; Processando...</h3>' });

			PagSeguroDirectPayment.getBrand({
				cardBin: $("#cardNumber").val(),
				success: function(response) {

					$("#creditCardBrand").val(response.brand.name);
					$("#cardNumber").css('border', '1px solid #999');

					$("#bandeiraCartao").attr('src', 'https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/68x30/' + response.brand.name + '.png');
					 $.unblockUI();
		  },
		  error: function(response) {
			$("#cardNumber").css('border', '2px solid red');
			$("#cardNumber").focus();
			  $.unblockUI();
		  },
		  complete: function(response) {
			  $.unblockUI();
		  }
		});
	 }
  }

</script>
<?}else{?>
	<div class="container">
		<div class="c-content-title-4">
			<h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-grey-1">Faça o login para acessar</span></h3>
			<div class="c-body c-center">
			</div>
		</div>
	</div>
<?}?>
</div>
