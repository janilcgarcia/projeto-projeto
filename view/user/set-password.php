<div class="container">
  <h2>Alterar Senha</h2>
  <?php if ($errors) { ?>
    <div class="alert alert-danger">
      <ul>
        <?php foreach ($errors as $error) { ?>
          <li><?= $error ?></li>
        <?php } ?>
      </ul>
    </div>
  <?php } ?>
  <form method="post" action="/?controller=User&action=setPassword">
    <input type="hidden" name="user" value="<?= $user_id ?>" />
    <input type="hidden" name="validity" value="<?= $validity ?>" />
    <input type="hidden" name="verification" value="<?= $digest ?>" />
    <div class="form-group">
      <label for="input-password" class="hide">Nova Senha</label>
      <input type="password" class="form-control c-square" id="input-password"
             name="password" placeholder="Nova Senha" />
    </div>

    <div class="form-group">
      <label for="input-confirm-password" class="hide">Confirmar Senha</label>
      <input type="password" class="form-control c-square"
             id="input-confirm-password" name="confirm-password"
             placeholder="Confirmar Senha" />
    </div>

    <div class="form-group">
      <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">
        Salvar
      </button>
    </div>
  </form>
</div>
