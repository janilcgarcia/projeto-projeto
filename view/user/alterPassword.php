<!DOCTYPE html>
<!-- 
Theme: JANGO - Ultimate Multipurpose HTML Theme Built With Twitter Bootstrap 3.3.7
Version: 2.0.1
Author: Themehats
Site: http://www.themehats.com
Purchase: http://themeforest.net/item/jango-responsive-multipurpose-html5-template/11987314?ref=themehats
Contact: support@themehats.com
Follow: http://www.twitter.com/themehats
-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"  >
<!--<![endif]-->
<!-- BEGIN HEAD -->


	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Customer Edit Profile</h3>
			<h4 class="">Page Sub Title Goes Here</h4>
		</div>
		<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
															<li class="c-state_active">Jango Components</li>
									
		</ul>
	</div>
</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
		<div class="container">
			<div class="c-layout-sidebar-menu c-theme ">
			<!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
<div class="c-sidebar-menu-toggler">
	<h3 class="c-title c-font-uppercase c-font-bold">My Profile</h3>
	<a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
		<span class="c-line"></span> <span class="c-line"></span> <span class="c-line"></span>
	</a>
</div>

<ul class="c-sidebar-menu collapse " id="sidebar-menu-1">
	<li class="c-dropdown c-open">
		<a href="javascript:;" class="c-toggler">My Profile<span class="c-arrow"></span></a>
		<ul class="c-dropdown-menu">
			<li class="c-active">
				<a href="dashboard.php">My Dashbord</a>
			</li>
			
			<li class="">
				<a href="shop-customer-addresses.html">My Addresses</a>
			</li>
			
				<a href="shop-product-wishlist.html">My Wishlist</a>
			</li>
		</ul>
	</li>
</ul><!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
			</div>
			<div class="c-layout-sidebar-content ">
			<!-- BEGIN: PAGE CONTENT -->
			<div class="c-content-title-1">
	<h3 class="c-font-uppercase c-font-bold">Change Password</h3>
	<div class="c-line-left"></div>
</div>
<form class="c-shop-form-1" action="dashboard.php">
	<!-- BEGIN: ADDRESS FORM -->
	<div class="">
		
		<div class="row">
			<div class="form-group col-md-12">
				<label class="control-label">Current password</label>
				<input type="password" class="form-control c-square c-theme" placeholder="Password">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="control-label">New password</label>
				<input type="password" class="form-control c-square c-theme" placeholder="Password">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="control-label">Repeat new  password</label>
				<input type="password" class="form-control c-square c-theme" placeholder="Password">
				<p class="help-block">Hint: The password should be at least six characters long. <br />
					To make it stronger, use upper and lower case letters, numbers, and symbols like ! " ? $ % ^ & ).</p>
			</div>
		</div>
		<!-- END: PASSWORD -->
		<div class="row c-margin-t-30">
			<div class="form-group col-md-12" role="group">
				<button type="submit"  class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Submit</button>
				<button type="submit"  class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancel</button>
			</div>
		</div>
	</div>
	<!-- END: ADDRESS FORM -->
</form>			<!-- END: PAGE CONTENT -->
			</div>
		</div>
	</div>
	<!-- END: PAGE CONTAINER -->

	<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-6 -->
<a name="footer"></a>
<footer class="c-layout-footer c-layout-footer-6 c-bg-grey-1">

	<div class="container">

		<div class="c-prefooter c-bg-white">

			<div class="c-head">
				<div class="row">
					<div class="col-md-6">
						<div class="c-left">
							<div class="socicon">
								<a href="#" class="socicon-btn socicon-btn-circle socicon-solid c-bg-grey-1 c-font-grey-2 c-theme-on-hover socicon-facebook tooltips" data-original-title="Facebook" data-container="body"></a>
								<a href="#" class="socicon-btn socicon-btn-circle socicon-solid c-bg-grey-1 c-font-grey-2 c-theme-on-hover socicon-twitter tooltips" data-original-title="Twitter" data-container="body"></a>
								<a href="#" class="socicon-btn socicon-btn-circle socicon-solid c-bg-grey-1 c-font-grey-2 c-theme-on-hover socicon-youtube tooltips" data-original-title="Youtube" data-container="body"></a>
								<a href="#" class="socicon-btn socicon-btn-circle socicon-solid c-bg-grey-1 c-font-grey-2 c-theme-on-hover socicon-tumblr tooltips" data-original-title="Tumblr" data-container="body"></a>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="c-right">
							<h3 class="c-title c-font-uppercase c-font-bold">Download Mobile App</h3>
							<div class="c-icons">
								<a href="#" class="c-font-30 c-font-green-1 socicon-btn c-bg-grey-1-hover socicon-android tooltips" data-original-title="Android" data-container="body"></a>
								<a href="#" class="c-font-30 c-font-grey-3 socicon-btn c-bg-grey-1-hover socicon-apple tooltips" data-original-title="Apple" data-container="body"></a>
								<a href="#" class="c-font-30 c-font-blue-3 socicon-btn c-bg-grey-1-hover socicon-windows tooltips" data-original-title="Windows" data-container="body"></a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="c-line"></div>

			<div class="c-body">
				<div class="row">
					<div class="col-md-4 col-sm-6 col-xs-12">
						<ul class="c-links c-theme-ul">
							<li><a href="#">About Jango</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms &	Conditions</a></li>
							<li><a href="#">Delivery</a></li>
							<li><a href="#">Promotions</a></li>
							<li><a href="#">News</a></li>
						</ul>
						<ul class="c-links c-theme-ul">
							<li><a href="#">Blogs</a></li>
							<li><a href="#">Projects</a></li>
							<li><a href="#">Clients</a></li>
							<li><a href="#">Services</a></li>
							<li><a href="#">Features</a></li>
							<li><a href="#">Stats</a></li>
						</ul>
					</div>
					<div class="col-md-5 col-sm-6 col-xs-12">
						<div class="c-content-title-1 c-title-md">
							<h3 class="c-title c-font-uppercase c-font-bold">Latest Tweets</h3>
							<div class="c-line-left hide"></div>
						</div>
						<div class="c-twitter">
							<a class="twitter-timeline"
								href="https://twitter.com/themehats"
								data-tweet-limit="2" 
								data-chrome="noheader nofooter noscrollbar noborders transparent">
								Loading tweets by @themehats...
							</a>
						</div>
					</div>
					<div class="col-md-3 col-sm-12 col-xs-12">
						<div class="c-content-title-1 c-title-md">
							<h3 class="c-title c-font-uppercase c-font-bold">Contact Us</h3>
							<div class="c-line-left hide"></div>
						</div>
						<p class="c-address c-font-16">
							25, Lorem Lis Street, Orange <br/> California, US <br/> Phone: 800 123 3456
							<br/> Fax: 800 123 3456 <br/> Email: <a href="mailto:info@jango.com"><span class="c-theme-color">info@jango.com</span></a>
							<br/> Skype: <a href="#"><span class="c-theme-color">jango</span></a>
						</p>
					</div>
				</div>
			</div>

			<div class="c-line"></div>

			<div class="c-foot">
				<div class="row">
					<div class="col-md-7">
						<div class="c-content-title-1 c-title-md">
							<h3 class="c-font-uppercase c-font-bold">About <span class="c-theme-font">JANGO</span></h3>
							<div class="c-line-left hide"></div>
						</div>
						<p class="c-text c-font-16 c-font-regular">Tolerare unus ducunt ad brevis buxum. Est alter buxum, cesaris. Eheu, lura! Racanas crescere in emeritis oenipons! Ubi est rusticus repressor? Lixa grandis clabulare est. Eposs tolerare.</p>
					</div>
					<div class="col-md-5">
						<div class="c-content-title-1 c-title-md">
							<h3 class="c-font-uppercase c-font-bold">Subscribe to Newsletter</h3>
							<div class="c-line-left hide"></div>
						</div>
						<div class="c-line-left hide"></div>
						<form action="#">
							<div class="input-group input-group-lg c-square">
								<input type="text" class="form-control c-square c-font-grey-3 c-border-grey c-theme" placeholder="Your Email Here"/>
					        	<span class="input-group-btn">
					            	<button class="btn c-theme-btn c-theme-border c-btn-square c-btn-uppercase c-font-16" type="button">Subscribe</button>
					        	</span>
							</div>
						</form>
					</div>
				</div>
			</div>

		</div>

	</div>

	<div class="c-postfooter c-bg-dark-2">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12 c-col">
					<p class="c-copyright c-font-grey">2015 &copy; JANGO
						<span class="c-font-grey-3">All Rights Reserved.</span>
					</p>
				</div>
									<div class="col-md-6 col-sm-12 c-col">
						<ul class="list-unstyled list-inline pull-right">
							<li><img class="img-responsive" src="assets/base/img/content/payments/light/1.png" width="60" /></li>
							<li><img class="img-responsive" src="assets/base/img/content/payments/light/2.png" width="60" /></li>
							<li><img class="img-responsive" src="assets/base/img/content/payments/light/22.png" width="60" /></li>
							<li><img class="img-responsive" src="assets/base/img/content/payments/light/5.png" width="60" /></li>
							<li><img class="img-responsive" src="assets/base/img/content/payments/light/6.png" width="60" /></li>
						</ul>
					</div>
							</div>
		</div>
	</div>

</footer>
<!-- END: LAYOUT/FOOTERS/FOOTER-6 -->

	<!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
<div class="c-layout-go2top">
	<i class="icon-arrow-up"></i>
</div><!-- END: LAYOUT/FOOTERS/GO2TOP -->

	<!-- BEGIN: LAYOUT/BASE/BOTTOM -->
    <!-- BEGIN: CORE PLUGINS -->
	<!--[if lt IE 9]>
	<script src="assets/global/plugins/excanvas.min.js"></script> 
	<![endif]-->
	<script src="assets/plugins/jquery.min.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery-migrate.min.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery.easing.min.js" type="text/javascript" ></script>
	<script src="assets/plugins/reveal-animate/wow.js" type="text/javascript" ></script>
	<script src="assets/demos/default/js/scripts/reveal-animate/reveal-animate.js" type="text/javascript" ></script>

	<!-- END: CORE PLUGINS -->

			<!-- BEGIN: LAYOUT PLUGINS -->
			<script src="assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
			<script src="assets/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
			<script src="assets/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
			<script src="assets/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
			<script src="assets/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
			<script src="assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
			<script src="assets/plugins/js-cookie/js.cookie.js" type="text/javascript"></script>
				<!-- END: LAYOUT PLUGINS -->
	
	<!-- BEGIN: THEME SCRIPTS -->
	<script src="assets/base/js/components.js" type="text/javascript"></script>
	<script src="assets/base/js/components-shop.js" type="text/javascript"></script>
	<script src="assets/base/js/app.js" type="text/javascript"></script>
	<script>
	$(document).ready(function() {    
		App.init(); // init core    
	});
	</script>
	<!-- END: THEME SCRIPTS -->

		<!-- END: LAYOUT/BASE/BOTTOM -->
</body>
</html>