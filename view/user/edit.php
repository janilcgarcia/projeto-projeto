<!DOCTYPE html>

<html lang="en"  >
<!--<![endif]-->
<!-- BEGIN HEAD -->




	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Customer Edit Profile</h3>
			<h4 class="">Page Sub Title Goes Here</h4>
		</div>
		<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
															<li class="c-state_active">Jango Components</li>
									
		</ul>
	</div>
</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
		<div class="container">
			<div class="c-layout-sidebar-menu c-theme ">
			<!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
<div class="c-sidebar-menu-toggler">
	<h3 class="c-title c-font-uppercase c-font-bold">My Profile</h3>
	<a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
		<span class="c-line"></span> <span class="c-line"></span> <span class="c-line"></span>
	</a>
</div>

<ul class="c-sidebar-menu collapse " id="sidebar-menu-1">
	<li class="c-dropdown c-open">
		<a href="javascript:;" class="c-toggler">My Profile<span class="c-arrow"></span></a>
		<ul class="c-dropdown-menu">
			<li class="c-active">
				<a href="dashboard.php">My Dashbord</a>
			</li>
			
			
		</ul>
	</li>
</ul><!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
			</div>
			<div class="c-layout-sidebar-content ">
			<!-- BEGIN: PAGE CONTENT -->
			<div class="c-content-title-1">
	<h3 class="c-font-uppercase c-font-bold">Edit Profile</h3>
	<div class="c-line-left"></div>
</div>
<form class="c-shop-form-1" >
	<!-- BEGIN: ADDRESS FORM -->
	<div class="">
		<!-- BEGIN: BILLING ADDRESS -->
		
		div class="row">
			<div class="col-md-12">
		<input type="hidden" name="id" value="<?= $user->getId() ?>" />
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				
						<label class="control-label">Name</label>
						<input type="text" name = "name" value="<?=$user->getName()?>"class="form-control c-square c-theme" >
					
			</div>
		</div>
		
		
		<div class="row">
			<div class="col-md-12">
				
						<label class="control-label">CPF</label>
						<input type="text" name="cpf" value="<?=$user->getCpf()?>" class="form-control c-square c-theme" >
					
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="form-group col-md-6">
						<label class="control-label">Email Address</label>
						<input type="email" class="form-control c-square c-theme" >
					</div>
					<div class="col-md-6">
						<label class="control-label">Phone</label>
						<input type="tel" name="phone" value="<?=$user->getPhone()?>" class="form-control c-square c-theme" >
					</div>
				</div>
			</div>
		</div>
		<!-- END: BILLING ADDRESS -->
		<!-- BEGIN: PASSWORD -->
		<div class="row">
			<div class="form-group col-md-12">
				<label class="control-label">Confirm  Password</label>
				<input type="password" class="form-control c-square c-theme" placeholder="Password">
			</div>
		</div>
		
		<!-- END: PASSWORD -->
		<div class="row c-margin-t-30">
			<div class="form-group col-md-12" role="group">
				<button type="submit" onclick="shop-customer-dashboard.html" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Submit</button>
				<button type="submit" onclick="shop-customer-dashboard.html" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancel</button>
			</div>
		</div>
	</div>
	<!-- END: ADDRESS FORM -->
</form>	
    <a href="?controller=User&action=deactivate" class="btn c-theme-btn btn-xs"><i class="fa fa-edit"></i> Deactivate account</a>
<!-- END: PAGE CONTENT -->
			<!-- END: PAGE CONTENT -->
			</div>
		</div>
	</div>
	<!-- END: PAGE CONTAINER -->

	<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-6 -->
<a name="footer"></a>


	<!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
<div class="c-layout-go2top">
	<i class="icon-arrow-up"></i>
</div><!-- END: LAYOUT/FOOTERS/GO2TOP -->

	<!-- BEGIN: LAYOUT/BASE/BOTTOM -->
    <!-- BEGIN: CORE PLUGINS -->
	<!--[if lt IE 9]>
	<script src="assets/global/plugins/excanvas.min.js"></script> 
	<![endif]-->
	<script src="assets/plugins/jquery.min.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery-migrate.min.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery.easing.min.js" type="text/javascript" ></script>
	<script src="assets/plugins/reveal-animate/wow.js" type="text/javascript" ></script>
	<script src="assets/demos/default/js/scripts/reveal-animate/reveal-animate.js" type="text/javascript" ></script>

	<!-- END: CORE PLUGINS -->

			<!-- BEGIN: LAYOUT PLUGINS -->
			<script src="assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
			<script src="assets/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
			<script src="assets/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
			<script src="assets/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
			<script src="assets/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
			<script src="assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
			<script src="assets/plugins/js-cookie/js.cookie.js" type="text/javascript"></script>
				<!-- END: LAYOUT PLUGINS -->
	
	<!-- BEGIN: THEME SCRIPTS -->
	<script src="assets/base/js/components.js" type="text/javascript"></script>
	<script src="assets/base/js/components-shop.js" type="text/javascript"></script>
	<script src="assets/base/js/app.js" type="text/javascript"></script>
	<script>
	$(document).ready(function() {    
		App.init(); // init core    
	});
	</script>
	<!-- END: THEME SCRIPTS -->

		<!-- END: LAYOUT/BASE/BOTTOM -->
</body>
</html>