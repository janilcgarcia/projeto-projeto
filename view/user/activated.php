<div class="container">
  <div class="col-xs-12">
    <h3>Parabéns!</h3>
    <p>Seu usuário foi ativado com sucesso, <?= $user->getName() ?>!</p>
    <p>Você pode voltar para a <a href="/">Página Inicial</a></p>
  </div>
</div>
