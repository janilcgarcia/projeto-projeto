
<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Produto</h3>
			<h4 class="">Adicionar novo Produto</h4>
		</div>
	</div>
</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
		<!-- BEGIN: PAGE CONTENT -->
		<div class="c-content-box c-size-lg">
	<div class="container">

		<form role="form" class="c-shop-form-1" action="index.php?controller=Manager&action=add" method="POST" enctype="multipart/form-data">
			<div class="row">
				<!-- BEGIN: ADDRESS FORM -->
				<div class="col-md-7 c-padding-20">
					<!-- BEGIN: BILLING ADDRESS -->
					<h3 class="c-font-bold c-font-uppercase c-font-24">Produtos</h3>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6">
									<label class="control-label">Nome</label>
									<input name="name" id="name" type="text" class="form-control c-square c-theme" placeholder="Informe o nome do produto">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-12">
							<label class="control-label">Descrição</label>
							<input name="description" id="description" type="text" class="form-control c-square c-theme" placeholder="Informe uma descrição sobre o produto">
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-12">
							<label class="control-label">Preço</label>
							<input name="price" id="price" type="text" class="form-control c-square c-theme" placeholder="Informe o preço">
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-6">
							<label class="control-label">Imagem</label>
							<input name="picture" id="picture" type="text" class="form-control c-square c-theme" placeholder="Insira o link da imagem">
						</div>
            <div class="row">
  						<div class="form-group col-md-6">
                <label class="control-label">Categoria</label>
                <select name="category_id" id="category_id" class="form-control">

                  <?php
                  $categoryDao = new CategoryDao();
                  $id = 1;
                  foreach($categories as $key => $category) : ?>

                  <option value="<?=$id?>"><?=$categoryDao->getCategory($id)?></option>
                  <?$id=$id+1?>

                  <?
                  endforeach;
                  ?>
                </select>
  						</div>
            </div>
					</div>

					<div class="row">
						<div class="form-group col-md-6">
							<label class="control-label">Quantidade</label>
							<input name="quatity" id="quatity" type="text" class="form-control c-square c-theme" placeholder="Informe a quantidade">
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-12" role="group">
							<button type="submit" name="save" id="save" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Adicionar </button>
							<a href="javascript:history.back()" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancelar</a>
						</div>
					</div>

				</div>
				<!-- END: ADDRESS FORM -->
				<!-- BEGIN: ORDER FORM -->
				</div>
				<!-- END: ORDER FORM -->
			</div>
		</form>
	</div>
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"
	            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	            crossorigin="anonymous"></script>
							<!-- Adicionando Javascript -->
