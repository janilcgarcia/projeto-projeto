


<div class="c-content-box c-size-lg c-bg-white">

  <?
    $users = array();
    $userDao = New UserDao();

    $loginManager = UserLoginManager::getInstance();

    $show='';
    $showEmpty='';

    if($loginManager->isLoggedIn()){
      $id =  array_key_exists ('id', $_GET) ? $_GET['id'] : 0;
      $user = $userDao->getUserById($id);

      if($user->isStaff()){
        $show = '';
        $showEmpty='hidden';
      }else{
        $show='hidden';
        $showEmpty='';
      }
    }

  ?>
<div class="container">
<? if ($loginManager->isLoggedIn()) {?>
<!-- BEGIN: CONTENT/SHOPS/SHOP-2-1 -->
<div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space c-bg-grey-1">
  <div class="container">
    <div class="c-content-title-4">
      <h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-grey-1">Histórico de compras</span></h3>
      <div class="c-body c-center">
      </div>
    </div>
  </div>

  <div class="c-content-box c-size-lg">
  	<div class="container">
  		<div class="c-shop-cart-page-1">
  			<div class="row c-cart-table-title">
  				<div class="col-md-2 c-cart-image">
  					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Identificador da Compra</h3>
  				</div>
  				<div class="col-md-4 c-cart-desc">
  					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Data</h3>
  				</div>
  				<div class="col-md-2 c-cart-qty">
  					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Preço Total</h3>
  				</div>
  				<div class="col-md-2 c-cart-price">
  					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Método de Pagamento</h3>
  				</div>
  				<div class="col-md-1 c-cart-remove">
            <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Excluir</h3>
          </div>
  			</div>
  			<!-- BEGIN: SHOPPING CART ITEM ROW -->
        <?
          $checkouts = array();
          $checkoutDao = New CheckoutDao();

          $checkouts = $checkoutDao->getAll();
        ?>

  			<?php
         foreach($checkouts as $compras) :
           if($compras->getUserId() == $user->getId()):
          ?>

  			<div class="row c-cart-table-row">
  				<div class="col-md-2">
  					<h3 class="c-font-bold c-font-22 c-font-dark"><?= $compras->getId() ?></h3>
  					</div>
  				<div class="col-md-4">
  					<h3 class="c-font-bold c-font-22 c-font-dark"><?= date("Y-m-d", strtotime($compras->getDate()))?></h3>
	         </div>
  				<div class="col-md-2">
  					<h3 class="c-font-bold c-font-22 c-font-dark"><?= strval($compras->getTotal()) ?></h3>
  				</div>
  				<div class="col-md-2">
  					<h3 class="c-font-bold c-font-22 c-font-dark"><?= strval($compras->getPagamento()) ?></h3>
  				</div>
  				<div class="col-md-1 col-sm-12 c-cart-remove">
  					<h3 class="c-font-bold c-font-22 c-font-dark"><a href="index.php?controller=Manager&action=deleteBuy&idBuy=<?= $compras->getId() ?>&id=<?= $user->getId()?>" class="c-theme-link c-cart-remove-desktop">X</a></h3>
  				</div>
  			</div>
  			<?
          endif;
        endforeach;
        ?>
  			<!-- END: SHOPPING CART ITEM ROW -->
</div>

<div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space c-bg-grey-1" <?= $showEmpty?>>
  <div class="container">
    <div class="c-content-title-4">
      <h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-grey-1">Você não é um Gerenciador de Produtos</span></h3>
      <div class="c-body c-center">
      </div>
    </div>
  </div>
</div>

<div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space c-bg-grey-1" <?= $show?>>
	<div class="container">
    <div class="c-content-title-4">
        <h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-grey-1">Você é um Gerenciador de Produtos</span></h3>
        <div class="c-body c-center">
        </div>
    </div>
		<div class="c-content-title-4">
			<h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-grey-1">Gerenciar Produtos</span></h3>
		</div>
		<div class="row">

			<?php

			foreach($itens as $key => $itemStore) : ?>

			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="#" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Quantidade: <?=$itemStore->getQuantity() ?></a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?=$itemStore->getPicture() ?>);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim"><?= $itemStore->getName()?></p>
						<p class="c-price c-font-16 c-font-slim">R$<?= $itemStore->getPrice()?> &nbsp;

						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="index.php?controller=Manager&action=edit&idItem=<?= $itemStore->getId() ?>&id=<?= $user->getId()?>" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-blue-3-hover c-btn-product">Editar</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="index.php?controller=Manager&action=delete&idItem=<?= $itemStore->getId() ?>&id=<?= $user->getId()?>" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Remover</a>
						</div>
					</div>
				</div>
			</div>

		<?

		endforeach; ?>

    <div class="row">
      <div class="form-group col-md-12" role="group">
        <a href="index.php?controller=Manager&action=add&id=<?= $user->getId()?>" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Adicionar Produtos</a>
        <a href="index.php?controller=Manager&action=addCategory&id=<?= $user->getId()?>" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Adicionar Nova Categoria</a>
      </div>
    </div>

		</div>

	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-2-1 -->



	   </div>
	   </div>
    </div>
  <? }else{?>
    <div class="container">
      <div class="c-content-title-4">
        <h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-grey-1">Faça o login para acessar</span></h3>
        <div class="c-body c-center">
        </div>
      </div>
    </div>
  <? }?>
  </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
