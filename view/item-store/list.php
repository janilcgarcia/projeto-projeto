


<div class="c-content-box c-size-lg c-bg-white">


<div class="container">


<!-- BEGIN: CONTENT/SHOPS/SHOP-2-1 -->
<div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space c-bg-grey-1">
	<div class="container">
		<div class="c-content-title-4">
			<h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-grey-1">Destaques</span></h3>
		</div>
		<div class="row">

			<?php

			foreach($itens as $key => $itemStore) {
				if($itemStore->getQuantity() >= 5) {
				?>

			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="#" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Detalhes</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(<?=$itemStore->getPicture() ?>);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim"><?= $itemStore->getName()?></p>
						<p class="c-price c-font-16 c-font-slim">R$<?= $itemStore->getPrice()?> &nbsp;

						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="?controller=ItemStore&action=view&id=<?= $itemStore->getId() ?>" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Detalhes</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="?controller=ItemStore&action=add&id=<?= $itemStore->getId() ?>" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Comprar</a>
						</div>
					</div>
				</div>
			</div>

		<?
				}
			} ?>

		</div>

	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-2-1 -->









	</div>
	</div>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
