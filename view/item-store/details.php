<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Detalhes do produto <?= $item->getName() ?></h3>

		</div>

	</div>
</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
		<!-- BEGIN: PAGE CONTENT -->
		<!-- BEGIN: CONTENT/SHOPS/SHOP-PRODUCT-DETAILS-2 -->
<div class="c-content-box c-size-lg c-overflow-hide c-bg-white">
	<div class="container">
		<div class="c-shop-product-details-2">
			<div class="row">
				<div class="col-md-6">
					<div class="c-product-gallery">
						<div class="c-product-desc c-left">
							<div class="container">
								<img src="<?= $item->getPicture() ?>" >
							</div>
						</div>
						<div class="row c-product-gallery-thumbnail">
							<div class="col-xs-3 c-product-thumb">
								<img src="<?= $item->getPicture() ?>">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="c-product-meta">
						<div class="c-content-title-1">
							<h3 class="c-font-uppercase c-font-bold"><?= $item->getName()?></h3>
							<div class="c-line-left"></div>
						</div>

						<div class="c-product-review">
							<div class="c-product-rating">
								<i class="fa fa-star c-font-red"></i>
								<i class="fa fa-star c-font-red"></i>
								<i class="fa fa-star c-font-red"></i>
								<i class="fa fa-star c-font-red"></i>
								<i class="fa fa-star-half-o c-font-red"></i>
							</div>
							<div class="c-product-write-review">
								<a class="c-font-red" href="#">Avalie</a>
							</div>
						</div>
						<div class="c-product-price">R$<?= $item->getPrice() ?></div>
						<div class="c-product-short-desc">
						<!-- Exibindo apenas uma parte da descricao -->
							<?= substr($item->getDescription(), 0, 200) . "..." ?>
							<a href="#div_desc">Ver mais</a>
						</div>
						<div class="c-product-add-cart c-margin-t-20">
							<div class="row">

								<div class="col-sm-12 col-xs-12 c-margin-t-20">
									<a href="index.php?controller=ShoppingCart&action=add&id=<?= $item->getId()?>" class="btn c-btn btn-lg c-font-bold c-font-white c-theme-btn c-btn-square c-font-uppercase">Adicionar ao carrinho</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-PRODUCT-DETAILS-2 -->

<!-- BEGIN: CONTENT/SHOPS/SHOP-PRODUCT-TAB-1 -->
<div class="c-content-box c-size-md c-no-padding" id="div_desc">
	<div class="c-shop-product-tab-1" role="tabpanel">
		<div class="container">
			<ul class="nav nav-justified" role="tablist">
				<li role="presentation" class="active">
					<a class="c-font-uppercase c-font-bold" href="#tab-1" role="tab" data-toggle="tab">Descrição</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase c-font-bold" href="#tab-2" role="tab" data-toggle="tab">Informações Adicionais</a>
				</li>

			</ul>
		</div>
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade active in" id="tab-1">
				<div class="c-product-desc c-center">
					<div class="container">
						<img src="<?= $item->getPicture() ?>">
						<p class="c-margin-t-20">
							<?= $item->getDescription()?>
						</p>

					</div>
				</div>
			</div>
			<!-- Informacoes adicionais dos produtos, caso necessario-->
			<div role="tabpanel" class="tab-pane fade" id="tab-2">
				<div class="container">
					<p class="c-center"><strong>Sizes:</strong> S, M, L, XL</p><br>
					<p class="c-center"><strong>Colors:</strong> Red, Black, Beige, White</p><br>
				</div>
				<div class="c-product-tab-meta-bg c-bg-grey c-center">
					<div class="container">
						<p class="c-product-tab-meta"><strong>SKU:</strong> 1410SL</p>
						<p class="c-product-tab-meta"><strong>Categories:</strong> <a href="#">Jacket</a>, <a href="#">Winter</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-PRODUCT-TAB-1 -->
