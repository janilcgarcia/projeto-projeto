<?

require('checkout/createSession.php');

setlocale(LC_MONETARY, 'pt_BR');

?>

<div class="c-layout-page">

<div class="c-layout-breadcrumbs-1 c-bgimage-full c-centered  c-fonts-uppercase c-fonts-bold   c-bg-img-center" style="background-image: url(theme/assets/base/img/content/backgrounds/bg-1.jpg);  background-size:cover; background-position:bottom;">
        <div class="c-breadcrumbs-wrapper">
            <div class="container">
                <div class="c-page-title c-pull-left">
                    <h3 class="c-font-uppercase c-font-bold c-font-white c-font-20 c-font-slim c-opacity-09">Check Out</h3>
                    <h4 class="c-font-white c-font-thin c-opacity-09">Simples e Seguro.</h4>
                </div>
                
            </div>
        </div>
    </div>                                                                                           
                                                                                            
<div class="c-content-box c-size-lg c-bg-white">
		
	<div style="padding: 20px;">

		<?php

		

		foreach ($warnings as $warning) { ?> <div class="alert alert-danger alert-dismissible" role="alert"><?= $warning?> </div> <? } 

		foreach ($messages as $message) { ?> <div class="alert alert-info alert-dismissible" role="alert"><?= $message ?></div><? } 

		?>
	</div>
	
	<div class="container">
	
		
		
		<form action="index.php?action=payment" id="accession" method="post" class="c-shop-form-1">
		
			

			<div class="row">
				<!-- BEGIN: ADDRESS FORM -->
				<div class="col-md-7 c-padding-20">

					<h3 class="c-font-bold c-font-uppercase c-font-24">Informe os seus dados</h3>

					<div class="row">
						<div class="form-group col-md-12">
							<label class="control-label">Nome</label>
							<input type="text"  class="form-control c-square c-theme" name="data[name]" value="" >
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-12">
							<label class="control-label">E-mail</label>
							<input type="email"  class="form-control c-square c-theme" name="data[email]" value="" >
						</div>
					</div>

					<h3 class="c-font-bold c-font-uppercase c-font-24">Dados do Cartão</h3>

					<div class="row">
					
						<div class="form-group col-md-9">
							<label class="control-label">Número</label>
								 <input type="text"  class="form-control  c-square c-theme creditCardNumber" name="data[cardNumber]" value="" id="cardNumber" onblur="brandCard();" />
						</div>
						<div class="col-md-3"  >
						<label class="control-label">Bandeira</label>
							<span class="form-control c-square">
								<img class="bandeiraCartao" id="bandeiraCartao" height="20" />
							</span>	

						</div>
					</div>

					<div class="row">
					
						<div class="form-group col-md-12">
							<div class="row" id="expiraCartao">
								<div class="form-group col-md-3">
									<label class="control-label">Mês de Vencimento </label>
									<input type="text"  class="form-control c-square c-theme day" name="data[cardExpirationMonth]" value="" id="cardExpirationMonth" maxlength="2" placeholder="<?= date('m')?>" /> 
								</div>
								<div class="form-group col-md-4">
									<label class="control-label">Ano de Vencimento </label>
									<input type="text"  class="form-control c-square c-theme year" name="data[cardExpirationYear]"  value="" id="cardExpirationYear"  maxlength="4" placeholder="<?= date('Y')?>"/>
								</div>
								<div class="col-md-4" id="cvvCartao">
									<label class="control-label">Código de Segurança</label>
									<input type="text"  class="form-control c-square c-theme creditCardCode" name="data[cardCvv]"  value="" id="cardCvv" maxlength="5" />
								</div>
							</div>
						</div>
					</div>

					<h3 class="c-font-bold c-font-uppercase c-font-24">Dados do Titular do Cartão </h3>

					<div class="row">
					<div class="form-group col-md-12">
							<div class="row" >
								<div class="form-group col-md-6">
									<input type="radio" name="data[holderType]" id="sameHolder" value="_SAME_" checked> &nbsp; Mesmo que o associado</input>
								</div>
								<div class="col-md-6" >
									<input type="radio" name="data[holderType]" id="otherHolder" value="_OTHER_"> &nbsp; Outro</input>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="form-group col-md-12">
							<label class="control-label">Nome (Como está impresso no cartão) </label>

							<input type="text"  class="form-control c-square c-theme" name="data[creditCardHolderName]"  value=""  id="creditCardHolderName" />
						</div>
					</div>
					
					<div class="row">
					
						<div class="form-group col-md-12">
							<div class="row" >
								<div class="form-group col-md-4">
									<label class="control-label">CPF (somente n&uacute;meros)</label>
										<input type="text" name="data[creditCardHolderCPF]"  value="" class="form-control c-square c-theme cpf"  id="creditCardHolderCPF" maxlength="14" />
								</div>
								<div class="col-md-8" >
									<label class="control-label">Data de Nascimento do Titular do Cartão </label>

										<input type="text"  class="form-control c-square c-theme date" name="data[creditCardHolderBirthDate]"  value=""  id="creditCardHolderBirthDate" maxlength="10" />
								</div>
							</div>
						</div>
					</div>
						
					<div class="row">
						<div class="form-group col-md-12">
							<div class="row" >
								<div class="form-group col-md-4">
									<label class="control-label">DDD</label>
										<input type="text" name="data[creditCardHolderAreaCode]" value="" id="creditCardHolderAreaCode" class="form-control c-square c-theme  phone_cod" maxlength="2" />
            					</div>
								<div class="col-md-8" >
									<label class="control-label">Número Telefone </label>

									<input type="text" name="data[creditCardHolderPhone]" value="" id="creditCardHolderPhone" class="form-control c-square c-theme  phone_number" maxlength="10" />
          						</div>
							</div>
						</div>
					</div>

					<h3 class="c-font-bold c-font-uppercase c-font-24">Endereço de Cobrança</h3>

					<div class="row">
					
						<div class="form-group col-md-12">
							<div class="row" >
								<div class="form-group col-md-8">
									<label class="control-label">Endereço</label>
										<input type="text"  class="form-control c-square c-theme"name="data[billingAddressStreet]"  value="" id="billingAddressStreet" />
								</div>
								<div class="col-md-4" >
									<label class="control-label">CEP</label>
									<input type="text"  class="form-control c-square c-theme cep"name="data[billingAddressPostalCode]"  value="" id="billingAddressPostalCode" maxlength="9"/>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
					
						<div class="form-group col-md-12">
							<div class="row" >
								<div class="form-group col-md-4">
									<label class="control-label">Número</label>
									<input type="text"  class="form-control c-square c-theme num" name="data[billingAddressNumber]"  value="" id="billingAddressNumber" maxlength="7"/>
							</div>
							<div class="col-md-8" >
								<label class="control-label">Bairro </label>

								<input type="text"  class="form-control c-square c-theme"name="data[billingAddressDistrict]"  value="" id="billingAddressDistrict" />
							</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-12">
							<label class="control-label">Complemento </label>
							<input type="text"  class="form-control c-square c-theme"name="data[billingAddressComplement]"  value="" id="billingAddressComplement" />
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-12">
							<div class="row" >
								<div class="form-group col-md-7">
									<label class="control-label">Cidade</label>
									<input type="text"  class="form-control c-square c-theme"name="data[billingAddressCity]"  value="" id="billingAddressCity" />
								</div>
								<div class="col-md-3" >
									<label class="control-label">Estado (Sigla) </label>

									<input type="text"  class="form-control c-square c-theme uf" name="data[billingAddressState]"  value="" id="billingAddressState"  maxlength="2" style="text-transform: uppercase;" placeholder="UF"/>
								</div>
								<div class="col-md-2" >
									<label class="control-label">País </label>

									<input type="text"  class="form-control c-square c-theme"name="data[billingAddressCountry]" id="billingAddressCountry" value="BRA" readonly />
								</div>
							</div>
						</div>
					</div>
					
					<input type="hidden" name="data[creditCardToken]" id="creditCardToken"  />
					<input type="hidden" name="data[creditCardBrand]" id="creditCardBrand"  />
					
					<div class="row">
    					<div class="form-group col-md-12" role="group">
    						<input type="hidden" name="data[senderHash]" id="senderHash"  />
							<button type="button" id="creditCardPaymentButton" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold" onclick="payment(); ">Confirmar Pagamento</button>
							<button type="button" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold" onClick="index.php">Continuar Comprando</button>
						</div>
					</div>	
				</div>

				<div class="col-md-5">
					<div class="c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
					<h1 class="c-font-bold c-font-uppercase c-font-24">Opções de Pagamento</h1>
					<ul class="c-order list-unstyled">
						
						
						<li class="row c-border-top c-margin-b-15"></li>
						<li class="row">
							<div class="col-md-7 c-font-16">Escolha a forma de parcelamento.</div>
							<div class="col-md-5">
								<div class="c-radio-list c-shipping-calculator" data-name="shipping_price" data-subtotal-selector="c-subtotal" data-total-selector="c-shipping-total">
									
									<div >
										<input type="radio" value="1" name="installment"> <b>À vista</b>
										
									</div>
									<div >
										<input type="radio" value="2" name="installment"> <b>2x</b> (sem juros)
										
									</div>
									<div >
										<input type="radio" value="3" name="installment"> <b>3x </b>(sem juros)
										
									</div>
									<div >
										<input type="radio" value="4" name="installment"> <b>4x </b>(sem juros)
										
									</div>
									<div >
										<input type="radio" value="5" name="installment""> <b>5x </b>(sem juros)
										
									</div>
								</div>
							</div>
						</li>
						
						<li class="row c-border-bottom"></li>
						
						

						<li class="row c-border-top c-margin-b-15"></li>
					
						<li class="row c-margin-b-15 c-margin-t-15">
							<div class="col-md-7 c-font-20">
								<p class="c-font-24">Total</p>
							</div>
							<div class="col-md-5 c-font-20">
								<p class="c-font-bold c-font-30">R$ <span class="c-shipping-total" id="total_value">0.00 </span></p>
								<input type="hidden" name="data[total]" id="input_total_value" value="">
							</div>
						</li>
						
						<li class="row">
							<div class="col-md-12">
								<div class="c-radio-list">
									<div class="c-radio">
										<input type="radio" id="radio3" class="c-radio" name="data[paymentMethod]" value="_CARD_" checked>
										<label for="radio3" class="c-font-bold c-font-20">
											<span class="inc"></span>
											<span class="check"></span>
											<span class="box"></span>
											Cartão de Crédito 
										</label>
										<img class="img-responsive" src="theme/assets/base/img/brand.png">
									</div>
									
									<div class="c-radio">
										<input type="radio" id="radio1" class="c-radio" name="data[paymentMethod]" value="_BOLETO_" >
										<label for="radio1" class="c-font-bold c-font-20">
											<span class="inc"></span>
											<span class="check"></span>
											<span class="box"></span>
											Boleto Bancário 
										</label>
										
									</div>
									
								</div>
							</div>
						</li>
						
						
						
						<li class="row c-margin-b-15 c-margin-t-15">
							<div class="form-group col-md-12">
								<div class="c-checkbox">
									<input type="checkbox" id="checkbox1-11" class="c-check">
									<label for="checkbox1-11">
										<span class="inc"></span>
										<span class="check"></span>
										<span class="box"></span>
										Li e concordo com os Termos &amp; Condições
									</label>
								</div>
							</div>
						</li>
						
						
						
						
						<li class="row">
							<div class="form-group col-md-12" role="group">
								<button type="submit" id="pagseguro" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Confimar </button>
								<button type="button" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold" onClick="document.location = '?action=checkout'">Continuar Comprando</button>
							</div>
						</li>
					</ul>
				</div>
				</div>
				
				<!-- END: ORDER FORM -->
			</div>
		</form>		  
	</div>
	</div>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>

<script type="text/javascript">
 	
 	PagSeguroDirectPayment.setSessionId('<?= $sessionCode->getResult() ?>');
	
</script>

<script>
	$(document).ready(function(){
	  	$('.date').mask('00/00/0000');
	  	$('.cep').mask('00000000');
	  	$('.phone_number').mask('0000000000');
		$('.phone_cod').mask('00');
	  	$('.cep').mask('99999-999');
		$('.num').mask('0000000');
		$('.creditCardNumber').mask('0000000000000000');
		$('.creditCardCode').mask('00000');
		$('.day').mask('00');
		$('.year').mask('0000');
		$('.uf').mask('AA');
	  	$('.cpf').mask('000.000.000-00');
		$('.money').mask('000.000.000.000.000,00', {reverse: true});
	});
</script>

<script>
	
	function payment()
	{	
		$.blockUI({ message: '<h3><img src="theme/assets/base/img/wait.gif"/> &nbsp; Processando...</h3>' });
		
		PagSeguroDirectPayment.createCardToken({
			cardNumber: $("#cardNumber").val(),
			brand: $("#creditCardBrand").val(),
			cvv: $("#cardCvv").val(),
			expirationMonth: $("#cardExpirationMonth").val(),
			expirationYear: $("#cardExpirationYear").val(),
			success: function (response) {
			  	$("#creditCardToken").val(response.card.token);
			  	$("#senderHash").val(PagSeguroDirectPayment.getSenderHash());
				$("#accession").submit();

				$.unblockUI();
        },
        error: function (response) {
          if (response.error) {

			  alert("Não foi possível validar o cartão. Preencha corretamente os dados e tente novamente.");
			  
			  $.unblockUI();
			  
          }
        },
        complete: function (response) {
        }
      });
	}
	
	function brandCard() {
    	
		if($("#cardNumber").val().length == 16)
		{
		
			$.blockUI({ message: '<h3><img src="theme/assets/base/img/wait.gif" /> &nbsp; Processando...</h3>' });

			PagSeguroDirectPayment.getBrand({
				cardBin: $("#cardNumber").val(),
				success: function(response) {

					$("#creditCardBrand").val(response.brand.name);
					$("#cardNumber").css('border', '1px solid #999');

					$("#bandeiraCartao").attr('src', 'https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/68x30/' + response.brand.name + '.png');
					 $.unblockUI();
		  },
		  error: function(response) {
			$("#cardNumber").css('border', '2px solid red');
			$("#cardNumber").focus();
			  $.unblockUI();
		  },
		  complete: function(response) {
			  $.unblockUI();
		  }
		});
	 }
  }
	
</script>
