<?php

class ProductController extends Controller
{
	private $productDao;

	public function __construct()
	{
		$this->view = new ProductView();

		$this->productDao = new ProductDao();
	}
	public function indexAction()
	{
		return;
	}

	public function listAction()
	{
		$viewModel = array(
			'products' => $this->productDao->getAll(),
			'categories' => $this->productDao->getCategories(),

		  );

		$this->setRoute($this->view->getListRoute());

		$this->showView($viewModel);

		return;

	}

	public function viewAction()
	{
		$id =  array_key_exists ('id', $_GET) ? $_GET['id'] : 0;

		$viewModel = array(
			'product' => $this->productDao->getProduct($id),
		  );

		$this->setRoute(ProductView::detailsRoute);

		$this->showView($viewModel);

		return;
	}

public function searchAction()
	{

		$name = array_key_exists ('name', $_REQUEST) ? $_REQUEST['name'] : '';
		$select = array_key_exists ('select', $_REQUEST) ? $_REQUEST['select'] : -1;
		if($select == -1){

			if($name == ''){
					$viewModel = array(
						'products' => $this->productDao->getAll(),
						'categories' => $this->productDao->getCategories(),
					);
			}else{
				$viewModel = array(
					'products' => $this->productDao->getSearch(-1, $name),
					'categories' => $this->productDao->getCategories(),
				);

			}

		}else{
			if($name == ''){
				$viewModel = array(
					'products' => $this->productDao->getSearch($select, -1),
					'categories' => $this->productDao->getCategories(),
				);

			}else{
				$viewModel = array(
					'products' => $this->productDao->getSearch($select, $name),
					'categories' => $this->productDao->getCategories(),
				);

			}

		}
		$this->setRoute($this->view->getListRoute());
		$this->showView($viewModel);

		return;
	}
}
