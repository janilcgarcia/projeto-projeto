<?php

class ShoppingCartController extends Controller
{
	private $itemStoreDao;

	public function __construct()
	{

		$this->view = new ItemStoreView();

		$this->itemStoreDao = new ItemStoreDao();

    $this->view = new CheckoutView();

    $this->checkoutDao = new CheckoutDao();
	}
	public function indexAction()
	{
		return;
	}

	public function addAction()
	{

    $message = Message::singleton();
		$shoppingCart = ShoppingCart::load();

		$id =  array_key_exists ('id', $_GET) ? $_GET['id'] : 0;
		$quantity = array_key_exists ('quantity', $_GET) ? $_GET['quantity'] : 1;

		$itemStore = $this->itemStoreDao->getItemStore($id);

    if($quantity <= 0){
      ShoppingCart::delete($id);
      $message->addWarning( $itemStore->getName() . ' foi removido do carrinho.');
    }else{
      ShoppingCart::add($itemStore, $quantity);

    }
		ShoppingCart::save();

    $viewModel = array(
        'checkouts' => $this->checkoutDao->getAll(),
    );

		$this->setRoute($this->view->getListRoute());

		$this->showView($viewModel);
	}

  public function deleteAction()
	{
    $message = Message::singleton();
		$shoppingCart = ShoppingCart::load();

		$id =  array_key_exists ('id', $_GET) ? $_GET['id'] : 0;

		$itemStore = $this->itemStoreDao->getItemStore($id);

		ShoppingCart::delete($id);

		ShoppingCart::save();

    $message->addWarning( $itemStore->getName() . ' foi removido do carrinho.');

    $viewModel = array(
        'checkouts' => $this->checkoutDao->getAll(),
    );

		$this->setRoute($this->view->getListRoute());

		$this->showView($viewModel);
	}

	public function listAction()
	{
		$viewModel = array(
			'itens' => $this->itemStoreDao->getAll(),
		  );

		$this->setRoute($this->view->getListRoute());

		$this->showView($viewModel);

		return ;
	}

	public function viewAction()
	{
		$id =  array_key_exists ('id', $_GET) ? $_GET['id'] : 0;

		$viewModel = array(
			'item' => $this->itemStoreDao->getItemStore($id),
		  );

		$this->setRoute(ItemStoreView::detailsRoute);

		$this->showView($viewModel);

		return;
	}
	public function freteAction()
	{

		$cep =  array_key_exists ('cep', $_REQUEST) ? $_REQUEST['cep'] : 0;

		$cep = trim($cep);
 		$cep = str_replace("-", "", $cep);

		$_args = array(
					 'nCdServico' => '40010',
					 'sCepOrigem'=>'79600080', 'sCepDestino' => $cep,
					 'nVlPeso' => '1', 'nVlComprimento' => 30,
					 'nVlAltura' => 15, 'nVlLargura' => 20  );

		$calculafrete = new CalculaFrete( $_args );

		$viewModel = array(
			'valorFrete' => $calculafrete->request(),
		  );

		$this->setRoute($this->view->getListRoute());

		$this->showView($viewModel);

		return;
	}
}
