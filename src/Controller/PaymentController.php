<?php

class PaymentController extends Controller
{
  private $paymentDao;

  public function __construct()
  {
    $this->view = new CheckoutView();

    $this->paymentDao = new PaymentDao();
  }
  public function indexAction()
  {
    return;
  }
  public function paymentAction()
  { 
    $message = Message::singleton();
    
    $viewModel = array();
    
    $this->setRoute($this->view->getCreateRoute()); 
    
    $this->showView($viewModel);

    return;
  }
}