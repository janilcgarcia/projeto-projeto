<?php

class ManagerController extends Controller
{
  private $managerDao;
  private $checkoutDao;

	public function __construct()
	{
		$this->view = new ManagerView();

		$this->managerDao = new ItemStoreDao();

    $this->checkoutDao = new CheckoutDao();
	}

  public function addAction()
  {
    $message = Message::singleton();

    $viewModel = array();

    $id =  array_key_exists ('id', $_REQUEST) ? $_REQUEST['id'] : 0;
		$select = array_key_exists ('category_id', $_REQUEST) ? $_REQUEST['category_id'] : -1;

   if(array_key_exists ('save', $_REQUEST))
   {
     $name =  array_key_exists ('name', $_REQUEST) ? $_REQUEST['name'] : '';
     $description = array_key_exists ('description', $_REQUEST) ? $_REQUEST['description'] : '';
     $price = array_key_exists ('price', $_REQUEST) ? $_REQUEST['price'] : 0;
     $picture = array_key_exists ('picture', $_REQUEST) ? $_REQUEST['picture'] : '';
     $category = array_key_exists ('category_id', $_REQUEST) ? $_REQUEST['category_id'] : 0;
     $quantity = array_key_exists ('quatity', $_REQUEST) ? $_REQUEST['quatity'] : 1;

     try
     {
       $itemStore = new ItemStore();
       $itemStore->setName($name);
       $itemStore->setDescription($description);
       $itemStore->setPrice($price);
       $itemStore->setPicture($picture);
       $itemStore->setCategoryId($category);
       $itemStore->setQuantity($quantity);

       $managerDao = new ItemStoreDao();
       $checkoutDao = new CheckoutDao();

       if($managerDao->insert($itemStore))
         $message->addMessage('Adicionado com sucesso!');

       $viewModel = array(
         'itens' => $managerDao->getAll(),
         'checkouts' => $this->checkoutDao->getAll(),
       );

       $this->view = new ItemStoreView();

       $this->setRoute($this->view->getListRoute());

     }
     catch(Exception $e)
     {
       $message->addWarning($e->getMessage());
     }
   }
   else
   {
     $managerDao = new ItemStoreDao();
     $checkoutDao = new CheckoutDao();

     $viewModel = array(
       'itens' => '$managerDao->getAll()',
       'checkouts' => $this->checkoutDao->getAll(),
     );

     $this->setRoute($this->view->getAddRoute());

   }

   $this->showView($viewModel);
  }

  public function editAction()
  {
    $id =  array_key_exists ('idItem', $_REQUEST) ? $_REQUEST['idItem'] : 0;

    $message = Message::singleton();

    if(array_key_exists ('save', $_REQUEST))
    {
     $name =  array_key_exists ('name', $_REQUEST) ? $_REQUEST['name'] : '';
     $description = array_key_exists ('description', $_REQUEST) ? $_REQUEST['description'] : '';
     $price = array_key_exists ('price', $_REQUEST) ? $_REQUEST['price'] : 0;
     $picture = array_key_exists ('picture', $_REQUEST) ? $_REQUEST['picture'] : '';
     $category = array_key_exists ('category_id', $_REQUEST) ? $_REQUEST['category_id'] : 0;
     $quantity = array_key_exists ('quatity', $_REQUEST) ? $_REQUEST['quatity'] : 1;

     try
     {
       $itemStore = new ItemStore();
       $itemStore->setId($id);
       $itemStore->setName($name);
       $itemStore->setDescription($description);
       $itemStore->setPrice($price);
       $itemStore->setPicture($picture);
       $itemStore->setCategoryId($category);
       $itemStore->setQuantity($quantity);

       $managerDao = new ItemStoreDao();
       $checkoutDao = new CheckoutDao();

        if($managerDao->update($itemStore))
          $message->addMessage('Editado com sucesso!');

        $viewModel = array(
          'itens' => $managerDao->getAll(),
          'checkouts' => $this->checkoutDao->getAll(),
        );

        $this->view = new ItemStoreView();

        $this->setRoute($this->view->getListRoute());
      }
      catch(Exception $e)
      {
        $message->addWarning($e->getMessage());
      }
    }
    else
    {
      $managerDao = new ItemStoreDao();
      $checkoutDao = new CheckoutDao();

      $viewModel = array(
        'itens' => $managerDao->getItemStore($id),
        'checkouts' => $this->checkoutDao->getAll(),
      );

      $this->setRoute($this->view->getEditRoute());
    }

    $this->showView($viewModel);
  }

  public function deleteAction()
  {
    $message = Message::singleton();

    $id =  array_key_exists ('idItem', $_REQUEST) ? $_REQUEST['idItem'] : 0;

    $managerDao = new ItemStoreDao();
    $checkoutDao = new CheckoutDao();

		$managerDao->delete($id);

		$viewModel = array(
			'itens' => $managerDao->getAll(),
      'checkouts' => $this->checkoutDao->getAll(),
		);

		$message->addMessage('Removido com sucesso!');

    $this->view = new ManagerView();

    $this->setRoute($this->view->getListRoute());

		$this->showView($viewModel);

		return;
  }

  public function listAction()
  {
    $viewModel = array(
			'itens' => $this->managerDao->getAll(),
      'checkouts' => $this->checkoutDao->getAll(),
		  );

		$this->setRoute($this->view->getListRoute());

		$this->showView($viewModel);

		return ;
  }

  public function deleteBuyAction()
  {
    $message = Message::singleton();

    $id =  array_key_exists ('idBuy', $_REQUEST) ? $_REQUEST['idBuy'] : 0;

    $checkoutDao = new CheckoutDao();
    $managerDao = new ItemStoreDao();

		$checkoutDao->delete($id);

		$viewModel = array(
      'itens' => $this->managerDao->getAll(),
      'checkouts' => $this->checkoutDao->getAll(),
		);

		$message->addMessage('Removido com sucesso!');

    $this->view = new ManagerView();

    $this->setRoute($this->view->getListRoute());

		$this->showView($viewModel);

		return;
  }

  public function addCategoryAction()
  {
    $message = Message::singleton();

    $viewModel = array();

    $id =  array_key_exists ('id', $_REQUEST) ? $_REQUEST['id'] : 0;

   if(array_key_exists ('save', $_REQUEST))
   {
     $name =  array_key_exists ('name', $_REQUEST) ? $_REQUEST['name'] : '';

     try
     {
       $category = new Category();
       $category->setName($name);

       $managerDao = new CategoryDao();
       $itens = new ItemStoreDao();

       if($managerDao->insert($category))
         $message->addMessage('Adicionado com sucesso!');

       $viewModel = array(
         'itens' => $itens->getAll(),
       );

       $this->view = new ItemStoreView();

       $this->setRoute($this->view->getListRoute());

     }
     catch(Exception $e)
     {
       $message->addWarning($e->getMessage());
     }
   }
   else
   {
     $managerDao = new ItemStoreDao();
     $productDao = new ProductDao();

     $viewModel = array(
       'itens' => '$managerDao->getAll()',
       'categories' => $productDao->getCategories(),
     );

     $this->setRoute($this->view->getAddCategoryRoute());

   }

   $this->showView($viewModel);
  }

}
?>
