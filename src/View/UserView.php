<?php

class UserView
{
	const detailsRoute = 'user/dashboard.php';

	public function __construct(){}

	public function getIndexRoute()
	{
		return 'user/index.php';
	}

	public function getUpdateUserRoute()
	{
		return 'user/edit.php';
	}

	public function getPasswordRoute()
	{
		return 'user/alterPassword.php';
	}

  public function getDashboardRoute()
  {
    return 'user/dashboard.php';
  }

	public function getRegisterRoute() {
		return 'user/register.php';
	}

	public function getActivateRoute() {
		return 'user/activated.php';
	}

	public function getLoginRoute() {
		return 'user/login.php';
	}

	public function getResetPasswordRoute() {
		return 'user/reset-password.php';
	}

	public function getResetSentRoute() {
		return 'user/reset-sent.php';
	}

	public function getInvalidPasswordResetRoute() {
		return 'user/invalid-password-reset.php';
	}

	public function getSetPasswordRoute() {
		return 'user/set-password.php';
	}

	public function getSucessfulRegistrationRoute() {
		return 'user/register-success.php';
	}
}
