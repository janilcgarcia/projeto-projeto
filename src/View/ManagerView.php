<?php

class ManagerView
{
	public function __contruct(){}

	public function getIndexRoute()
	{
		return 'checkout/index.php';
	}

	public function getListRoute()
	{
		return 'manager/listProduct.php';
	}

	public function getAddRoute()
	{
		return 'manager/addProduct.php';
	}

	public function getEditRoute()
	{
		return 'manager/editProduct.php';
	}

	public function getAddCategoryRoute()
	{
		return 'manager/addCategory.php';
	}

}
