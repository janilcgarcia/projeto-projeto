<?php

class ProductView
{
	const detailsRoute = 'product/details.php';

	public function __contruct(){}

	public function getIndexRoute()
	{
		return 'product/index.php';
	}

	public function getListRoute()
	{
		return 'product/list.php';
	}

	public function getDetailsRoute()
	{
		return 'product/details.php';
	}
}
