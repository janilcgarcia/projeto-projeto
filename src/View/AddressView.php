<?php

class AddressView
{
	public function __contruct(){}

	public function getIndexRoute()
	{
		return 'checkout/index.php';
	}

	public function getListRoute()
	{
		return 'address/list.php';
	}

	public function getAddRoute()
	{
		return 'address/add.php';
	}

	public function getEditRoute()
	{
		return 'address/edit.php';
	}

}
