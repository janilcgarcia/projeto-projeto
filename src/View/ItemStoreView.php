<?php

class ItemStoreView
{
	const detailsRoute = 'item-store/details.php';
	
	public function __contruct(){}
	
	public function getIndexRoute()
	{
		return 'item-store/index.php';
	}
	
	public function getListRoute()
	{
		return 'item-store/list.php';
	}
	
	public function getDetailsRoute()
	{
		return 'item-store/details.php';
	}
}