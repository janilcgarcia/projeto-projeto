<?php

class CategoryDao
{
  const _table = 'categories';

  public function __construct() { }

  public function getCategory($id)
  {
    $db = Database::singleton();

    $sql = 'SELECT * FROM ' . self::_table . ' WHERE id = ?';

    $sth = $db->prepare($sql);

    $sth->bindValue(1, $id, PDO::PARAM_STR);

    $sth->execute();

    if($obj = $sth->fetch(PDO::FETCH_OBJ))
    {
        return $obj->name;
    }

    return false;
  }

  public function getAll()
  {
    $db = Database::singleton();

    $sql = 'SELECT * FROM ' . self::_table;

    $sth = $db->prepare($sql);

    $sth->execute();

    $categories = array();

    while($obj = $sth->fetch(PDO::FETCH_OBJ))
    {
      $category = new Category();
      $category->setId($obj->id);
      $category->setName($obj->name);

      $categories[] = $category;
    }

    return $categories;
  }

  public function insert($category)
	{
		$db = Database::singleton();

		$message = Message::singleton();

		try{

			$db->beginTransaction();

			$sql = "INSERT INTO categories (name) VALUES (?)";

			$sth = $db->prepare($sql);
			$sth->bindValue(1, $category->getName(), PDO::PARAM_STR);

			if(!$sth->execute())
				return true;

			$db->commit();
		}
		catch(PDOException $e)
		{
			$db->rollBack();
			$message->addWarning($e->getMessage());
		}

		return false;
	}
}
