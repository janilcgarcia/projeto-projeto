<?php

class AddressDao
{
  const _table = 'addresses';

  public function __construct() { }

  public function getAll($id)
  {
    $db = Database::singleton();

    $sql = 'SELECT * FROM ' . self::_table . ' WHERE user_id = ?';

    $sth = $db->prepare($sql);

    $sth->bindValue(1, $id, PDO::PARAM_INT);

    $sth->execute();

    $addresses = array();

    while($obj = $sth->fetch(PDO::FETCH_OBJ))
    {
      $address = new Address();
      $address->setId($obj->id);
      $address->setUserId($obj->user_id);
      $address->setStreet($obj->street);
      $address->setNumber($obj->num);
      $address->setCep($obj->cep);
      $address->setCity($obj->city);
      $address->setState($obj->state);
      $address->setDistrict($obj->district);

      $addresses[] = $address;
    }

    return $addresses;
  }

  public function getAddress($id)
  {
    $db = Database::singleton();

    $sql = 'SELECT * FROM ' . self::_table . ' WHERE id = ?';

    $sth = $db->prepare($sql);

    $sth->bindValue(1, $id, PDO::PARAM_INT);

    $sth->execute();

    if($obj = $sth->fetch(PDO::FETCH_OBJ))
    {
        $address = new Address();
        $address->setId($obj->id);
        $address->setUserId($obj->user_id);
        $address->setStreet($obj->street);
        $address->setNumber($obj->num);
        $address->setCep($obj->cep);
        $address->setCity($obj->city);
        $address->setState($obj->state);
        $address->setDistrict($obj->district);


        return $address;
    }

    return false;
  }
  public function insert($address)
	{
		$db = Database::singleton();

		$message = Message::singleton();

		try{

			$db->beginTransaction();

			$sql = "INSERT INTO addresses (user_id, street, num, cep, district, city, state) VALUES (?, ?, ?, ?, ?, ?, ?)";

			$sth = $db->prepare($sql);
			$sth->bindValue(1, $address->getUserId(), PDO::PARAM_STR);
			$sth->bindValue(2, $address->getStreet(), PDO::PARAM_STR);
			$sth->bindValue(3, $address->getNumber(), PDO::PARAM_INT);
			$sth->bindValue(4, $address->getCep(), PDO::PARAM_STR);
			$sth->bindValue(5, $address->getDistrict(), PDO::PARAM_STR);
			$sth->bindValue(6, $address->getCity(), PDO::PARAM_STR);
			$sth->bindValue(7, $address->getState(), PDO::PARAM_STR);


			if(!$sth->execute())
				return true;

			$db->commit();
		}
		catch(PDOException $e)
		{
			$db->rollBack();
			$message->addWarning($e->getMessage());
		}

		return false;
	}

	public function update($address)
	{
		$db = Database::singleton();

		$message = Message::singleton();

		try{
			$db->beginTransaction();
			$sql = 'UPDATE addresses SET street = ?, num= ?, cep= ?, district=?, city=?, state=?  WHERE id = ?';

			$sth = $db->prepare($sql);

			$sth->bindValue(1, $address->getStreet(), PDO::PARAM_STR);
			$sth->bindValue(2, $address->getNumber(), PDO::PARAM_INT);
			$sth->bindValue(3, $address->getCep(), PDO::PARAM_STR);
			$sth->bindValue(4, $address->getDistrict(), PDO::PARAM_STR);
      $sth->bindValue(5, $address->getCity(), PDO::PARAM_STR);
      $sth->bindValue(6, $address->getState(), PDO::PARAM_STR);
      $sth->bindValue(7, $address->getId(), PDO::PARAM_INT);

			if(!$sth->execute())
				return true;

			$db->commit();
		}
		catch(PDOException $e)
		{
      $db->rollBack();
			$message->addWarning($e->getMessage());
		}

		return false;
	}
  public function delete($id)
	{
    $db = Database::singleton();

    $message = Message::singleton();

    try{

      $db->beginTransaction();

      $sql = "DELETE FROM addresses where id = ?";

      $sth = $db->prepare($sql);

      $sth->bindValue(1, $id, PDO::PARAM_INT);

      if(!$sth->execute())
		    return true;

      $db->commit();
    }
    catch(PDOException $e)
    {
      $db->rollBack();
      $message->addWarning($e->getMessage());
    }

    return false;
  }
}
