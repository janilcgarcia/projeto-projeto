<?
 class ItemStore
 {
 	private $id;

 	private $name;

 	private $description;

 	private $price;

	private $picture;

  private $categoryId;

  private $quantity;

 	public function __construct(){}

 	public function setId($id) { $this->id = $id; }

 	public function getId(){ return $this->id; }

 	public function setName($name){ $this->name = $name; }

 	public function getName() { return $this->name; }

 	public function setDescription ($description) { $this->description = $description; }

 	public function getDescription(){ return $this->description; }

 	public function setPrice($price) { $this->price = $price; }

 	public function getPrice() { return $this->price; }

	public function setPicture($picture) { $this->picture = $picture; }

 	public function getPicture() { return $this->picture; }

  public function setCategoryId($categoryId) { $this->categoryId = $categoryId; }

 	public function getCategoryId() { return $this->categoryId; }

  public function setQuantity($quantity) { $this->quantity = $quantity; }

  public function getQuantity() { return $this->quantity; }

 }
