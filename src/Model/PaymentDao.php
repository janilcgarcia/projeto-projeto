<?php

class PaymentDao
{
  const _table = 'payment';

  public function __construct() { }

  public function getAll()
  {
    
    $db = Database::singleton();

    $sql = 'SELECT * FROM ' . self::_table;
    
    $sth = $db->prepare($sql);

    $sth->execute();

    $users = array();
    
    while($obj = $sth->fetch(PDO::FETCH_OBJ))
    {
      $payment = new Payment();
      $payment->setId($obj->id);
     
      $payments[] = $payment; 
    }
    
    return $itens;  
  }

  public function getPayment($id)
  {
    $db = Database::singleton();

    $sql = 'SELECT * FROM ' . self::_table . ' WHERE id = ?';

    $sth = $db->prepare($sql);

    $sth->bindValue(1, $id, PDO::PARAM_STR);

    $sth->execute();

    if($obj = $sth->fetch(PDO::FETCH_OBJ))
    {
        $payment = new Payment();
        $payment->setId($obj->id);
        
        return $payment;
    }

    return false;
  }
}