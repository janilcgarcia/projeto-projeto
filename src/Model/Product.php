<?
 class Product
 {
 	private $id;

 	private $name;

 	private $description;

 	private $price;

	private $picture;

  private $category_id;

  private $quatity;

 	public function __construct(){}

 	public function setId($id) { $this->id = $id; }

 	public function getId(){ return $this->id; }

 	public function setName($name){ $this->name = $name; }

 	public function getName() { return $this->name; }

 	public function setDescription ($description) { $this->description = $description; }

 	public function getDescription(){ return $this->description; }

 	public function setPrice($price) { $this->price = $price; }

 	public function getPrice() { return $this->price; }

  public function setPicture($picture) { $this->picture = $picture; }

 	public function getPicture() { return $this->picture; }

  public function setCategory($category_id) { $this->category_id = $category_id; }

  public function getCategory() { return $this->category_id; }

  public function setQuatity($quatity) { $this->quatity = $quatity; }

  public function getQuatity() { return $this->quatity; }

 }
