<?php

class CheckoutDao
{
  const _table = 'checkout';

  public function __construct() { }

  public function getAll()
  {
    $db = Database::singleton();

    $sql = 'SELECT * FROM ' . self::_table;

    $sth = $db->prepare($sql);

    $sth->execute();

    $checkouts = array();

    while($obj = $sth->fetch(PDO::FETCH_OBJ))
    {
      $checkout = new Checkout();
      $checkout->setId($obj->id);
      $checkout->setDate($obj->cdate);
      $checkout->setTotal($obj->total);
      $checkout->setPagamento($obj->pagamento);
      $checkout->setUserId($obj->user_id);

      $checkouts[] = $checkout;
    }

    return $checkouts;
  }

  public function getCheckout($id)
  {
    $db = Database::singleton();

    $sql = 'SELECT * FROM ' . self::_table . ' WHERE id = ?';

    $sth = $db->prepare($sql);

    $sth->bindValue(1, $id, PDO::PARAM_STR);

    $sth->execute();

    if($obj = $sth->fetch(PDO::FETCH_OBJ))
    {
        $checkout = new Checkout();
        $checkout->setId($obj->id);
        $checkout->setDate($obj->cdate);
        $checkout->setTotal($obj->total);
        $checkout->setPagamento($obj->pagamento);
        $checkout->setUserId($obj->user_id);

        return $checkout;
    }

    return false;
  }

  public function insert($checkout)
  {
    $db = Database::singleton();

    $message = Message::singleton();

    try{

      $db->beginTransaction();

      $sql = "INSERT INTO checkout (cdate, total, pagamento, user_id) VALUES (?, ?, ?, ?)";

      $sth = $db->prepare($sql);
      $sth->bindValue(1, $checkout->getDate(), PDO::PARAM_STR);
      $sth->bindValue(2, $checkout->getTotal(), PDO::PARAM_INT);
      $sth->bindValue(3, $checkout->getPagamento(), PDO::PARAM_STR);
      $sth->bindValue(4, $checkout->getUserId(), PDO::PARAM_INT);

      if(!$sth->execute())
        return true;

      $db->commit();
    }
    catch(PDOException $e)
    {
      $db->rollBack();
      $message->addWarning($e->getMessage());
    }

    return false;
  }

  public function delete($id)
  {
    $db = Database::singleton();

    $message = Message::singleton();

    try{

      $db->beginTransaction();

      $sql = "DELETE FROM checkout where id = ?";

      $sth = $db->prepare($sql);

      $sth->bindValue(1, $id, PDO::PARAM_INT);

      if(!$sth->execute())
		    return true;

      $db->commit();
    }
    catch(PDOException $e)
    {
      $db->rollBack();
      $message->addWarning($e->getMessage());
    }

    return false;
  }
}
