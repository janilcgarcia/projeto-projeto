<?
 class Address
 {
 	private $id;

 	private $user_id;

  private $street;

  private $number;

  private $cep;

  private $city;

  private $state;

  private $district;

  private $def;

 	public function getId() { return $this->id; }

 	public function setId($id) { $this->id = $id; }

 	public function getUserId() { return $this->user_id; }

 	public function setUserId($user_id) { $this->user_id =  $user_id; }

  public function getStreet() { return $this->street; }

  public function setStreet($street) { $this->street =  $street; }

  public function getNumber() { return $this->number; }

  public function setNumber($number) { $this->number =  $number; }

  public function getCep() { return $this->cep; }

  public function setCep($cep) { $this->cep =  $cep; }

  public function getCity() { return $this->city; }

  public function setCity($city) { $this->city =  $city; }

  public function getState() { return $this->state; }

  public function setState($state) { $this->state =  $state; }

  public function getDistrict() { return $this->district; }

  public function setDistrict($district) { $this->district =  $district; }

  public function getDef(){return $this->def;}

  public function setDef($def) {$this->def = $def;}
 }
