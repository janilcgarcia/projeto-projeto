CREATE TABLE categories
(
	id serial primary key,
	name varchar(200) not null
);

CREATE TABLE item_store
(
	id serial primary key,
	name varchar(70) not null,
	description varchar(120) not null,
	price numeric not null,
	picture varchar(200),
	category_id integer,
	quatity integer,
	foreign key (category_id) references categories(id)
);

create table users
(
	id serial primary key,
	name varchar(80) not null,
	email varchar(60) not null,
	password_hash varchar(256) not null,
	phone varchar(15) not null,
	notify_promotions boolean not null,
	newsletter boolean not null,
	cpf varchar(14) not null
);

create table addresses
(
	id serial primary key,
	street varchar(50) not null,
	num integer not null,
	cep varchar(9) not null,
	district varchar(50),
	city varchar(50),
	state varchar(2),
	user_id integer,
	foreign key (user_id) references users(id)
);

CREATE TABLE checkout
(
  id serial primary key,
  cdate date,
  total numeric,
  pagamento character varying(100),
  user_id integer
);
