
INSERT INTO users(name, email, password_hash, phone, notify_promotions, newsletter, cpf)
values('Teste', 'seuemaildopagsegurosandbox@email.com', '123', '(11)11111-11111', '0', '0', '111.111.111-75');

INSERT INTO addresses(street, num, cep, district, city, state, user_id)
values('Rua Nove de Julho', '123', '16900-415', 'Benfica', 'Andradina', 'SP', '1');

INSERT INTO addresses(street, num, cep, district, city, state, user_id)
values('Rua Nove de Julho', '123456', '16900-415', 'Benfica', 'Andradina', 'SP', '1');

/*Faz qualquer insert na tabela category com id*/

INSERT INTO item_store(name, description, price, picture, category_id)
values('PRODUTO', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.', '1200.00', 'uploads/product-picture/item1-picture1.png', '1');

INSERT INTO item_store(name, description, price, picture, category_id)
values('PRODUTO 2', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.', '100.00', 'uploads/product-picture/item2-picture1.jpg', '1');
