<?php

class Renderer
{
	public static function render($viewModel = false, $route)
	{
		if($viewModel)
			foreach ($viewModel as $key => $value) eval('$'. $key . ' = $value;');
		
		include('view/default/header.php');

		//include('view/default/menu.php');

		include('view/helper/message.php');
		
		include('view/' . $route);
		
		include('view/default/footer.php');
	}
}