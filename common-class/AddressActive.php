<?php

class AddressActive
{
	private static $address;

	public static function get()
	{
		return self::$address;
	}

	public static function load()
	{
		if(isset($_SESSION['ADDRESS_ACTIVE']))
			self::$address = unserialize($_SESSION['ADDRESS_ACTIVE']);
	}

	public static function save ()
	{
		$_SESSION['ADDRESS_ACTIVE'] = serialize(self::$address);

	}

	public static function select ($address)
	{

		self::$address = $address;
		

	}

}
